function getCookieValue(key) {
	var cookies = document.cookie;
	// lastLoginDate=1508919505213;SEESION=FADLFDAJDAF
	var cookieArr = cookies.split(";"); // 将字符串按照;进行分割成数组
	for (var i = 0; i < cookieArr.length; i++) {// 对分割后的数组进行遍历
		// lastLoginDate=1508919505213
		var cookieI = cookieArr[i];
		var cookieIArr = cookieI.split("="); // 将遍历到的一个用=连接的键值对分割成一个长度为2数组
		if (cookieIArr[0] == key) {// cookieIArr[0]为cookie的key
			return cookieIArr[1];// cookieIArr[1]为cookie的value
		}
	}
	return null;
}


function getUserByIdIndex(userId) {
	if (userId != null) {
		$.getJSON("/YiQiBang/UserServlet", {
			action : "getUserById",
			userid : userId
		}, function(data) {
			if (data.retCode == 0) {
				//</span><a href="/YiQiBang/UserServlet?action=logoutUser">注销</a>
				$("#loginBtn").text(data.retData.nickname);
				$("#loginBtn").attr('data-toggle',"collapse");
						$("#loginBtn").attr('data-target',"#collapseExample");
								$("#loginBtn").attr('aria-expanded',"false")
								$("#loginBtn").attr('aria-controls',"collapseExample");

								$("#loginBtn").append('<div class="collapse" id="collapseExample">'
  +'<div class="well">'
  + '<p onclick="loginOut()">注销</p>'
  +'</div>'
+'</div>');
				$("#loginBtn").css("color","white");
			} 
		});
	}
}


function loginOut(){
	$.getJSON("/YiQiBang/UserServlet",{
		action:"logoutUser"
	},function(data){
		if(data.retCode==0){
			window.location.href="login.html";
		}else{
			alert("注销失败");
		}
	});
}