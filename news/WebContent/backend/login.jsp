<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html;charset=UTF-8"%>
<%
	String appPath = request.getContextPath();
	
%>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<link href="<%=appPath%>/backend/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="<%=appPath%>/backend/css/login.css" rel="stylesheet">
<script src="<%=appPath%>/backend/jquery/jquery-1.11.1.min.js"></script>
<script src="<%=appPath%>/backend/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=appPath%>/backend/js/login.js"></script>
<script type="text/javascript">
var path="/news";


/**
 * User login
 * @returns {Boolean}
 */
function checkLoginUser(){
	var username=$("#userName").val();
	var password=$("#userPwd").val();
	var validateCode=$("#userValidate").val();
	if(username==""||password==""){
		$("#tip").text("Username or password cannot be empty");
		return false;
	}else if(validateCode==""){ 
		$("#tip").text("Please enter verification code");
		return false;
	}else{
		$('#ProgressModal').modal('show');
		// Get user list
		$.getJSON(
			path + "/AdminServlet",
			{
				action : "loginAdmin",
				username:username,
				password:password,
				userValidate:validateCode
			},
			function(data) {
				if (data.retCode == 0) {
					window.location.href="index.jsp";
				}else if(data.retCode==1001){
					$("#userValidate").val("");
					$("#tip").text("Verification code error");
				}else if(data.retCode==1002){
					$("#tip").text("Username or password is incorrect");
				}else if(data.retCode==1003){
					$("#tip").text("This user is not an administrator, please contact another administrator");
				}else{
					$("#tip").text("Login failed");
				}
				refreshCode();
				$('#ProgressModal').modal('hide');
				
		});
	}
}
</script>
</head>
<body>
	<div>
		<h2></h2>
		<form id="form2">
		 <img src="img/newslogo.jpg" id="right_img">
			<div class="form-group first" >
				<label for="userName">Username：</label> <input type="text"
					class="form-control" id="userName" name="userName"
					placeholder="Please enter user name" >
			</div>
			<div class="form-group">
				<label for="userPwd">Password：</label> <input
					type="password" class="form-control" id="userPwd" name="userPwd"
					placeholder="Please enter password">
			</div>
			<!--Verification code-->
			<div class="form-grup" id="validate_box">
				<label for="userName">Verification code：</label>
				<div class="media">
					<div class="media-left">
						<img onclick="refreshCode()" src="validate.jsp" id="123"
							alt="Can't see the picture? Click to get the verification code again." style="cursor:hand;">
					</div>
					<div class="media-body">
						<input type="text" class="form-control" id="userValidate"
							name="userValidate" placeholder="Verification code">
					</div>
				</div>
			</div>
			<p id="tip"></p>
			<div class="btn_box">
				<button type="button" class="btn btn-default btn-sm" onclick="checkLoginUser()">Login</button>
				<button type="button" class="btn btn-default btn-sm left_margin">Forget password</button>
			</div>
		</form>

	</div>
 <jsp:include page="progress_bar.jsp"></jsp:include>
</body>
<script>
	function refreshCode() {
		$("#123").attr("src", "validate.jsp?abc=" + Math.random());
	}
</script>
</html>
