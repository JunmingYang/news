<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String appPath = request.getContextPath();
	if (request.getSession().getAttribute("loginAdmin") == null) {
		response.sendRedirect(appPath + "/backend/login.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News crawler</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<!--Introducing a common css style file -->
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="<%=appPath%>/backend/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="<%=appPath%>/backend/css/common.css" />
<script src="<%=appPath%>/backend/jquery/jquery-1.11.1.min.js"></script>
<script src="<%=appPath%>/backend/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=appPath%>/backend/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$("#header .list-group a").eq(3)
				.attr("class", "list-group-item active");
	});
</script>
</head>
<body>
	<div id="header">
		<jsp:include page="../header.jsp" />
		<div class="container-fluid" id="content">
			<div class="row">
				<div class="col-md-2" id="left_nav">
					<jsp:include page="../left.jsp" />
				</div>
				<div class="col-md-10" id="right_content">
					<!--News crawler-->
					<h2 class="text-center" style="color:lightgray;font-size:30px">Function has not been opened yet</h2>
				</div>
			</div>
		</div>
	</div>
</body>

</html>