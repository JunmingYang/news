<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8" import="news.pojo.*"%>
<%
	String appPath = request.getContextPath();
	Admin user=(Admin)session.getAttribute("loginAdmin");
	
	int userid=-1;
	if (user == null) {
		response.sendRedirect(appPath+"/backend/login.jsp");
	}else{
		userid=user.getId();
	}
	
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Comment management</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<!--Introducing a common css style file -->
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="<%=appPath%>/backend/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="<%=appPath%>/backend/css/common.css" />
<script src="<%=appPath%>/backend/jquery/jquery-1.11.1.min.js"></script>
<script src="<%=appPath%>/backend/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=appPath%>/backend/js/common.js" type="text/javascript"></script>
<script src="<%=appPath%>/backend/js/date_util.js"></script>
<script type="text/javascript">
	var path = "/news";
	$(function() {
		$("#header .list-group a").eq(2)
				.attr("class", "list-group-item active");
		getEvaluateList();
		getNewsList();

		/*Add or update comments*/
		$("#sureAddOrEditBtn").click(function() {
			var result = checkAdd();
			if (result) {
				var data = {
					evaid:$("#editIt").val(),
					newsid : $("#newsid").val(),
					userid : <%=userid%>,
					state : ($("#showEva").prop("checked") ? 1 : 0),
					content : $("#evaluateContent").val()
				}
				if ($("#cop_title").text() == "Edit comment") {
					//Edit comment
					$.getJSON(path + "/UserEvaluateServlet?action=updateUserEvaluate", data, function(data) {
						if (data.retCode == 0) {
							getEvaluateList();
							$('#commAddModal').modal('hide');
						} else {
							alert("The modification failed, please try again");
							$('#commAddModal').modal('hide');
						}
					});
				} else {
					//Add comment
					$.getJSON(path + "/UserEvaluateServlet?action=addUserEvaluate", data, function(data) {
						if (data.retCode == 0) {
							getEvaluateList();
							$('#commAddModal').modal('hide');
						} else {
							alert("Add failed, please try again");
							$('#commAddModal').modal('hide');
						}
					});
				}
			}
			$("#exampleModalLabel").reset();
		});
		/*Delete comment*/
		$("#sureDelComm").click(function() {
			var delId = $("#delId").val();
			if (delId != -1) {
				$.getJSON(path + "/UserEvaluateServlet", {
					action : "delUserEvaluate",
					evaid : delId
				}, function(data) {
					if (data.retCode == 0) {
						getEvaluateList();
					} else {
						alert("Delete failed, please try again");
					}
					$('.danger_alert').css("display", "none");
				});
			} else {
				alert("Failed to delete parameter, please try again");
				$('.danger_alert').css("display", "none");
			}
		});
	});
	function getEvaluateList(){
		$('#ProgressModal').modal('show');
		$
				.getJSON(
						path + "/UserEvaluateServlet",
						{
							action : "userEvaluateList"
						},
						function(data) {
							if (data.retCode == 0) {
								var evaluateList = data.retData;
								listResultHandle(evaluateList);
							}
						});
	}
	
	function listResultHandle(evaluateList){
		$("#userEvaluateListBody").html("");
		if(evaluateList.length==0){
			$("#userEvaluateListBody").append('<h2 class="text-center" style="color:lightgray;font-size:30px;">Get result is empty</h2>');
			$('#ProgressModal').modal('hide');
		}else{
			for (var i = 0; i < evaluateList.length; i++) {
				var evaluate = evaluateList[i];
				var userHtml = '<tr id="userTr'+i+'">'
						+ '<td>'
						+ evaluate.id
						+ '</td>'
						+ '<td id="eval_news'+i+'">'
						+ '</td>'
						+ '<td id="eval_user'+i+'">'
						+ '</td>'
						+ '<td>'
						+ evaluate.content
						+ '</td>'
						+ '<td>'
						+ evaluate.commenttime
						+ '</td>'
						+ '<td>'
						+ (evaluate.type == 1 ? "Show":"Hide")
						+ '</td>'
						+ '<td class="icon">'
						+ '<div class="caozuo_grid row ">'
						+ '<div class="col-md-4 col-md-offset-2 caozuo_update" id="caozuo_update'+i+'"></div>'
						+ '<div class="col-md-4 caozuo_del" onclick="delComm('
						+ evaluate.id
						+ ')">'
						+ '</div>'
						+ '</div>' + '</td>' + '</tr>';

				$("#userEvaluateListBody").append(userHtml);
				getNewById(evaluate.newsid,i);
				getUserById(evaluate.userid,i);
			}
			$('#ProgressModal').modal('hide');
			$(".caozuo_update").each(function(i){
				$("#caozuo_update"+i).click(
						function() {
							var evaluate = evaluateList[i];
							$("#cop_title").text("Edit comment");
							$("#exampleModalLabel .tip").text("");
							$("#editIt").val(evaluate.id);
							$("#newsid").val(evaluate.newsid);
							$("#evaluateContent").val(evaluate.content);
							if(evaluate.type==1){
								$("#showEva").prop("checked","checked");
							}else{
								$("#hideEva").prop("checked","checked");
							}
							$('#commAddModal').modal('show');
				});
			});
			
		}
		
	}
	/*Get news list*/
	function getNewsList() {
		$.getJSON(path + "/NewsServlet", {
			action : "newsList"
		}, function(data) {
			var newsList = data.retData;
			for (var i = 0; i < newsList.length; i++) {
				var news = newsList[i];
				$("#newsid").append(
						'<option value="'+news.id+'">' + news.title
								+ '</option>');
			}
		});
	}
	/**
	       Get news details based on id
	 */
	function getNewById(newId,index) {
		if (newId != null) {
			$.getJSON(path + "/NewsServlet", {
				action : "getNewById",
				id : newId
			}, function(data) {
				if (data.retCode == 0) {
					$("#eval_news" +index).text(data.retData.title);
				}else{
					$("#eval_news" + index).text("Deleted");
				}
			});
		}
	}
	/*Get details based on user id
	 */
	function getUserById(userId,index) {
		if (userId != null) {
			$.getJSON(path + "/UserServlet", {
				action : "getUserById",
				userid : userId
			}, function(data) {
				if (data.retCode == 0) {
					$("#eval_user" + index).text(data.retData.username);
				}else{
					$("#eval_user" + index).text("Deleted");
				}
			});
		}
	}
	
	// Add comment field verification
	function checkAdd() {
		var evaluateContent = $("#evaluateContent").val();
		if (evaluateContent == "") {
			$("#exampleModalLabel .tip").text("Please fill in the comments！");
			return false;
		}
		return true;
	}
	/*Delete news box*/
	function delComm(id) {
		$("#delId").val(id);
		$('.danger_alert').css("display", "block");
	}
	function cancelDel() {
		$('.danger_alert').css("display", "none");
	}

	function addCommBtn() {   
		if (<%=userid%>!=null) {
			$("#cop_title").text("Add comment");
			$("#exampleModalLabel .tip").text("");
			$('#commAddModal').modal('show');
		} else {
			window.location.href = "../login.jsp";
			return;
		}

	}
	
	function searchLike(){
		var searchstr=$("#searchStr").val();
		$('#ProgressModal').modal('show');
		if(searchstr==""){
			getEvaluateList();
		}else{
			$
			.getJSON(
					path + "/UserEvaluateServlet",
					{
						action : "searchEvaByLike",
						searchStr : searchstr
					},
					function(data) {
						if (data.retCode == 0) {
							var evaluateList = data.retData;
							listResultHandle(evaluateList);
					    }else{
							alert("No query results！");
							getEvaluateList();
							$('#commAddModal').modal('hide');
						}
						$('#ProgressModal').modal('hide');
						
				});
			}
	}
</script>
</head>
<body>
	<div id="header">
		<jsp:include page="../header.jsp" />
		<div class="container-fluid" id="content">
			<div class="row">
				<div class="col-md-2" id="left_nav">
					<jsp:include page="../left.jsp" />
				</div>
				<div class="col-md-10" id="right_content">
				
					<form class="form-horizontal row" id="search_row">
					
						<div class="col-md-3">
							<div class="input-group" id="search_box">
								<input type="text" class="form-control" placeholder="Comments" id="searchStr">
								<span class="input-group-btn">
									<button class="btn btn-default search_btn" type="button" onclick="searchLike()" style="font-size:0">
										<span class="glyphicon glyphicon-search input_icon"></span>
									</button>
								</span>
							</div>
							<!-- /input-group -->
						</div>
						<div class=" col-md-offset-8 col-md-1 text-right">
							<button type="button" class="btn btn-default btn-sm orange_icon" id="addBtn"
								onclick="addCommBtn()" data-target=".bs-example-modal-sm">
								<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
								Add
							</button>
						</div>
					</form>
					<table class="table table-bordered text-center" cellspacing="0"
						id="data_table">
						<thead>
							<td>Serial number</td>
							<td>Comment news</td>
							<td>Comment user</td>
							<td>Comment content</td>
							<td>Comment time</td>
							<td>state</td>
							<td>Operation</td>
						</thead>
						<tbody id="userEvaluateListBody">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Delete warning box-->
	<div class="danger_alert alert alert-danger alert-dismissible fade in"
		role="alert">
		<h4>Delete warning</h4>
		<p>Are you sure you want to delete this user?</p>
		<p class="btn_tip">
			<button type="button" class="btn btn-xs btn-danger" id="sureDelComm">Delete</button>
			<button type="button" class="btn btn-xs btn-default"
				onclick="cancelDel()">Cancel</button>
			<input type="hidden" value="-1" id="delId">
		</p>
	</div>
	<!--Add category popup-->
	<div id="commAddModal" class="modal fade bs-example-modal-sm"
		tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="cop_title">Add comment</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="exampleModalLabel">
						<div class="form-group">
							<label for="categoryname" class="col-md-2 control-label">Comment news:</label>
							<div class="col-md-6">
								<select class="form-control" id="newsid" name="newsid">
								</select>
							</div>
							<input type="hidden" value="-1" id="editIt">
						</div>
						<div class="form-group">
							<label for="contents" class="col-md-2 control-label">Content:</label>
							<div class="col-md-6">
								<textarea style="width: 100%; height: 150px"
									id="evaluateContent"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="contents" class="col-md-2 control-label">State:</label>
							<div class="col-md-6">
								<label class="radio-inline"> <input type="radio"
									name="state" id="showEva" value="1" checked> Show
								</label> <label class="radio-inline"> <input type="radio"
									name="state" id="hideEva" value="0"> Hide
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-6">
								<span class="tip" style="color: red"></span>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-default" data-dismiss="modal"
						form="exampleModalLabel" onclick="cancelDel()">
						<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
						Cancel
					</button>
					<button type="button" class="btn btn-default" id="sureAddOrEditBtn">
						<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../progress_bar.jsp"></jsp:include>
</body>
</html>