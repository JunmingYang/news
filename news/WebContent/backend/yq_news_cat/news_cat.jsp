<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String appPath = request.getContextPath();
	if (session.getAttribute("loginAdmin") == null) {
		response.sendRedirect(appPath + "/backend/login.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News classification management</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<!--Introducing a common css style file -->
<link href="<%=appPath%>/backend/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="<%=appPath%>/backend/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="<%=appPath%>/backend/css/common.css" />
<script src="<%=appPath%>/backend/jquery/jquery-1.11.1.min.js"></script>
<script src="<%=appPath%>/backend/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=appPath%>/backend/js/date_util.js"
	type="text/javascript"></script>
<script src="<%=appPath%>/backend/js/common.js" type="text/javascript"></script>
<script>
	var path = "/news";
	$(function() {
		$("#header .list-group a").eq(4).addClass("active");
		getCategoryList();
		/*Add Category - Update Category*/
		$("#sureAddOrEditBtn").click(function() {
			var catName = $("#categoryname").val();
			if (!isNaN(catName) || catName.trim() == "") {
				alert("Please enter a reasonable classification name");
				return;
			} else if ($("#cop_title").text() == "Modification type") {
				var editIt = $("#editIt").val();
				$.ajax({
					url : path + "/CategoryServlet",
					type : "post",
					data : {
						action : "updateCategory",
						categoryid : editIt,
						categoryname : catName
					},
					success : function(data) {
						location.reload();
					},
					error : function(e) {
						alert("The modification failed, please try again");
					}
				});
			} else {
				$.ajax({
					url : path + "/CategoryServlet",
					type : "post",
					data : {
						action : "addCategory",
						categoryname : catName
					},
					success : function(data) {
						location.reload();
					},
					error : function(e) {
						alert("Add failed, please try again");
					}
				});
			}
			$("#exampleModalLabel").reset();
		});
		/*Delete category*/
		$("#sureDelCat").click(function() {
			var delId = $("#delId").val();
			if (delId != -1) {
				$.getJSON(path + "/CategoryServlet", {
					action : "removeCategory",
					categoryid : delId
				}, function(data) {
					if (data.retCode == 0) {
						getCategoryList();
					} else {
						alert("Failed to get delete parameter, please try again");
					}
					$('.danger_alert').css("display", "none");
				});
			} else {
				alert("Failed to get delete parameter, please try again");
				$('.danger_alert').css("display", "none");
			}
		});
	});

	function getCategoryList() {
		$("#catListBody").html("");
		$('#ProgressModal').modal('show');
		$.getJSON(
				path + "/CategoryServlet",
				{
					action : "getCategoryList"
				},function(data) {
						console.log(data);
						if (data.retCode == 0) {
							var catList = data.retData;
							if (catList.length == 0) {
								$("#newsListBody").append('<h2 class="text-center" style="color:lightgray;font-size:30px;">Get result is empty</h2>');
								$('#ProgressModal').modal('hide');
							} else {
								for (var i = 0; i < catList.length; i++) {
									var cat = catList[i];
									var userHtml = '<tr id="catTr'+i+'">'
											+ '<td>'
											+ cat.id
											+ '</td>'
											+ '<td>'
											+ cat.newsname
											+ '</td>'
											+ '<td>'
											+ cat.registertime
											+ '</td>'
											+ '<td class="icon">'
											+ '<div class="caozuo_grid row ">'
											+ '<div class="col-md-4 col-md-offset-2 caozuo_update" onclick="updateCategory('
											+ cat.id
											+ ',&apos;'
											+ cat.newsname
											+ '&apos;)"></div>'
											+ '<div class="col-md-4 caozuo_del" onclick="delCategory('
											+ cat.id
											+ ')">'
											+ '</div>'
											+ '</div>'
											+ '</td>'
											+ '</tr>';
									$("#catListBody").append(userHtml);
								}
							}
						}
						$('#ProgressModal').modal('hide');
					});
	}
	/*Delete classification prompt box*/
	function delCategory(id) {
		$("#delId").val(id);
		$('.danger_alert').css("display", "block");
	}
	function cancelDel() {
		$('.danger_alert').css("display", "none");
	}
	/* Modify type prompt box */
	function updateCategory(id, name) {
		$("#cop_title").text("Modification type");
		$("#editIt").val(id);
		$("#categoryname").val(name);
		$('#catAddModal').modal('show');
	}
	function addCatBtn() {
		//$("#cop_title").text("Add type");
		$("#categoryname").val(""); //
		$('#catAddModal').modal('show');
	}

	function getCategorysByLike() {
		$("#catListBody").html("");
		var likeStr = $("#searchStr").val();
		if (likeStr == "") {
			getCategoryList();
		} else {
			$('#ProgressModal').modal('show');
			$
					.getJSON(
							path + "/CategoryServlet",
							{
								action : "getCategorysByLike",
								cateStr : likeStr
							},
							function(data) {
								if (data.retCode == 0) {
									var catList = data.retData;
									for (var i = 0; i < catList.length; i++) {
										var cat = catList[i];
										var userHtml = '<tr id="catTr'+i+'">'
												+ '<td>'
												+ cat.id
												+ '</td>'
												+ '<td>'
												+ cat.newsname
												+ '</td>'
												+ '<td>'
												+ cat.registertime
												+ '</td>'
												+ '<td class="icon">'
												+ '<div class="caozuo_grid row ">'
												+ '<div class="col-md-4 col-md-offset-2 caozuo_update" onclick="updateCategory('
												+ cat.id
												+ ',&apos;'
												+ cat.newsname
												+ '&apos;)"></div>'
												+ '<div class="col-md-4 caozuo_del" onclick="delCategory('
												+ cat.id
												+ ')">'
												+ '</div>'
												+ '</div>'
												+ '</td>'
												+ '</tr>';

										$("#catListBody").append(userHtml);
									}
								} else {
									alert("Failed to get, please try again");
								}
								$('#ProgressModal').modal('hide');
							});
		}

	}
</script>
</head>
<body>
	<div id="header">
		<jsp:include page="../header.jsp" />
		<div class="container-fluid" id="content">
			<div class="row">
				<div class="col-md-2" id="left_nav">
					<jsp:include page="../left.jsp" />
				</div>
				<div class="col-md-10" id="right_content">
					<!--News classification management-->
					<form class="form-horizontal row" id="search_row">

						<div class="col-md-3">
							<div class="input-group" id="search_box">
								<input type="text" class="form-control" placeholder="Enter search content"
									id="searchStr"> <span class="input-group-btn">
									<button class="btn btn-default" type="button"
										class="search_btn" onclick="getCategorysByLike()">
										<span class="glyphicon glyphicon-search input_icon"
											aria-hidden="true"></span>
									</button>
								</span>
							</div>
							<!-- /input-group -->
						</div>
						<div class=" col-md-offset-8 col-md-1 text-right">
							<button type="button" class="btn btn-default btn-sm orange_icon"
								id="addBtn" onclick="addCatBtn()">
								<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
								Add
							</button>
						</div>
					</form>
					<table class="table table-bordered text-center" cellspacing="0"
						id="data_table">
						<thead>
							<td>Serial number</td>
							<td>Name</td>
							<td>Creation time</td>
							<td>Operating</td>
						</thead>
						<tbody id="catListBody" align="center">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Delete warning box-->
	<div class="danger_alert alert alert-danger alert-dismissible fade in"
		role="alert">
		<h4>Delete warning</h4>
		<p>Are you sure you want to delete this user??</p>
		<p class="btn_tip">
			<button type="button" class="btn btn-xs btn-danger" id="sureDelCat">Delete</button>
			<button type="button" class="btn btn-xs btn-default"
				onclick="cancelDel()">Cancel</button>
			<input type="hidden" value="-1" id="delId">
		</p>
	</div>
	<!--Add category popup-->
	<div id="catAddModal" class="modal fade bs-example-modal-sm"
		tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="cop_title">Add category</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="exampleModalLabel">
						<div class="form-group">
							<label for="categoryname" class="col-md-2 control-label">Category Name:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="categoryname"
									name="categoryname">
							</div>
							<input type="hidden" value="-1" id="editIt">
						</div>

					</form>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-default" data-dismiss="modal"
						form="exampleModalLabel" onclick="cancelDel()">
						<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
						Cancel
					</button>
					<button type="button" class="btn btn-default" id="sureAddOrEditBtn">
						<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../progress_bar.jsp"></jsp:include>
</body>
</html>