<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String appPath = request.getContextPath();
%>
<<script type="text/javascript">
	$(function() {
		$("#left_nav").css("min-height", $(window).height() + "px");
		$("#left_nav>.list-group").css("min-height",
				$(window).height() - 200 + "px");
	});
</script>

<div class="list-group">
	<a href="<%=appPath%>/backend/index.jsp" class="list-group-item"><img
		src="<%=appPath%>/backend/img/yonghuguanli.png"><span>User management</span></a>
	<a href="<%=appPath%>/backend/yq_news/news.jsp" class="list-group-item"><img
		src="<%=appPath%>/backend/img/xinwenguanli.png"><span>News management</span></a>
	<a href="<%=appPath%>/backend/yq_comment/comment.jsp"
		class="list-group-item active"><img
		src="<%=appPath%>/backend/img/pinglunguanli.png"><span>Comment management</span></a>
	<a href="<%=appPath%>/backend/yq_crawler/crawler.jsp"
		class="list-group-item"><img
		src="<%=appPath%>/backend/img/xinwenpachong.png"><span>News crawler</span></a>
	<a href="<%=appPath%>/backend/yq_news_cat/news_cat.jsp"
		class="list-group-item"><img
		src="<%=appPath%>/backend/img/xinwenfenleiguanli.png"><span>News classification management</span></a>
</div>
<div id="logo_img"></div>
