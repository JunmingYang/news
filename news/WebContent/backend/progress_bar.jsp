<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
 	String appPath = request.getContextPath();
 %> 

<div id="ProgressModal" class="modal fade bs-example-modal-sm" data-backdrop="false"
	tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
	style="background:rgba(0,0,0,0.5)">
	<div class="modal-dialog" role="document" style="margin-top:300px">
		<div class="progress">
			<div class="progress-bar progress-bar-striped active"
				role="progressbar" aria-valuenow="100" aria-valuemin="0"
				aria-valuemax="100" style="width: 100%">
				<span class="sr-only">100%</span>
			</div>
		</div>
	</div>
	<div class="text-center" style="font-size:22px;color:white;">Loading, please wait...</div>
</div>
	
