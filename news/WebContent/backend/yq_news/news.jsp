<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String appPath = request.getContextPath();
	if (session.getAttribute("loginAdmin") == null) {
		response.sendRedirect(appPath+"/backend/login.jsp");
	} 

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News management</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<!--Introducing a common css style file -->
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="<%=appPath%>/backend/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="<%=appPath%>/backend/css/common.css" />
<script src="<%=appPath%>/backend/jquery/jquery-1.11.1.min.js"></script>
<script src="<%=appPath%>/backend/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=appPath%>/backend/js/common.js" type="text/javascript"></script>
<script src="<%=appPath%>/backend/js/date_util.js"></script>
<style>
.news_title, .news_content {
	text-over: ellipsis;
	white-space: nowrap;
}

.img_box img {
	width: 120px;
	height: 130px;
	display: none;
}

.img_box input[type=button] {
	margin-top: 10px;
}

td.imgI>img {
	width: 70px;
	height: 70px;
}
</style>
<script type="text/javascript">
	var path = "/news";
	$(function() {
		$("#header .list-group>.list-group-item").eq(1).addClass("active");
		
		getNewsList();
		/*Get all news categories*/
		$.getJSON(path + "/CategoryServlet", {
			action : "getCategoryList"
		}, function(data) {
			if (data.retCode == 0) {
				var catList = data.retData;
				for (var i = 0; i < catList.length; i++) {
					var cat = catList[i];
					$("#newsCategory").append(
							'<option value="'+cat.id+'">'
									+ cat.newsname + '</option>');
				}
			}
		});

		//Add or modify news OK button events
		$("#sureAddOrEditBtn").click(function() {
			if ($("#cop_title").text() == "Modify news") {
				updateNews();
			} else {
				addNews();
			}
		});
		
		/*Modify news events*/
		function updateNews(){
			$('#newsAddModal').modal('show');
			var result = checkAdd();
			if (result) {
				var data = {
					action : "updateNew",
					id:$("#editId").val(),
					title : $("#title").val(),
					source : $("#newsSource").val(),
					newsCategory : $("#newsCategory").val(),
					content : $("#newsContent").val(),
					newsPic1 : $("#newsPic1").val(),
					
					newsPic2 : $("#newsPic2").val(),
				}
				 $.getJSON(path + "/NewsServlet", data, function(data) {
					if (data.retCode == 0) {
						getNewsList();
						location.reload();
						$('#newsAddModal').modal('hide');
						alert("Successfully modified");
					}else {
						alert("The modification failed, please try again");
						$('#newsAddModal').modal('hide');
					}
				}); 
			}
		}
		/*Delete News*/
		$("#sureDelNews").click(function() {
			var delId = $("#delId").val();
			if (delId != -1) {
				$.getJSON(path + "/NewsServlet", {
					action : "delNew",
					id : delId
				}, function(data) {
					if (data.retCode == 0) {
						location.reload();
					} else {
						alert("Delete failed, please try again");
					}
				});
			} else {
				alert("Failed to delete parameter, please try again");
			}
		});

	});

	//Get a news list
	function getNewsList(){
		$('#ProgressModal').modal('show');
		$("#newsListBody").html("");
		$.getJSON(
				path + "/NewsServlet",
				{
					action : "newsList"
				},
				function(data) {
					if (data.retCode == 0) {
						var newsList = data.retData;
						showNewsList(newsList);
						$('#ProgressModal').modal('hide');
					}
				});
	}
	
	function showNewsList(newsList){
		if(newsList.length==0){
			$("#newsListBody").append('<h2 class="text-center" style="color:lightgray;font-size:30px;">Get result is empty</h2>');
			$('#ProgressModal').modal('hide');
		}else{
			for (var i = 0; i < newsList.length; i++) {
				var news = newsList[i];
				var userHtml = '<tr class="newsTr" id="newsTr'+i+'">'
						+ '<td>'
						+ news.id
						+ '</td>'
						+ '<td class="news_title" title="'+news.title+'">'
						+ news.title
						+ '</td>'
						+ '<td>'
						+ news.source
						+ '</td>'
						+ '<td id="category_news'+i+'">'
						+ +'</td>'
						+ '<td>'
						+ news.readcount
						+ '</td>'
						+ '<td>'
						+ news.commentcount
						+ '</td>'
						+ '<td class="imgI">'
						+ '<img alt="None" src="<%=appPath%>/NewsServlet?action=downloadNewPic&newsPic1='+news.pic1+'">'
						+ '</td>'
						+ '<td class="imgI">'
						+ '<img alt="None" src="<%=appPath%>/NewsServlet?action=downloadNewPic&newsPic1='+news.pic2+'">'
						+ '</td>'
						+ '<td>'
						+ news.registertime
						+ '</td>'
						+ '<td>'
						+ (news.modifytime == "" ? "No operation" : news.modifytime)
						+ '<td class="news_content" title="'+news.content+'">'
						+ news.content
						+ '</td>'
						+ '<td class="icon">'
						+ '<div class="caozuo_grid row ">'
						+ '<div class="col-md-4 col-md-offset-2 caozuo_update" id="updateNews'+news.id+'"></div>'
						+ '<div class="col-md-4 caozuo_del" onclick="delNews('
						+ news.id
						+ ')">'
						+ '</div>'
						+ '</div>' + '</td>' + '</tr>';
	
				$("#newsListBody").append(userHtml);
				getCategoryById(news.classifyid,i);
		}
		
			/*Modify news click event listener*/
			$(".caozuo_update").each(function(i){
				$(".caozuo_update").eq(i).click(function(){
					var news = newsList[i];
					$("#cop_title").text("Modify news");
					$("#editId").val(news.id);
					$("#title").val(news.title);
					$("#newsSource").val(news.source);
					$("#newsCategory").val();
					$("#newsContent").val(news.content);
					$("#exampleModalLabel .tip").text("");
					$("#img_ipt").val("");
					
					$(".img_ipt").css("display","block");
					
					$(".btn_upimg").css({"display":"block","text-align":"center"});
					$(".btn_upimg").val("Modify");
					/*Set picture*/
					if(news.pic1!=-1){
						$(".img_box img").eq(0).css("display","block");
						var srcPath="<%=appPath%>/NewsServlet?action=downloadNewPic&newsPic1="+news.pic1+"";
						$(".img_box img").eq(0).attr("src",srcPath);
					}else{
						$(".img_box img").eq(0).css("display","block");
						$(".img_box img").eq(0).attr("src","../img/avatar_def.jpg");
					}
					
					if(news.pic2!=-1){
						$(".img_box img").eq(1).css("display","block");
						var srcPath="<%=appPath%>/NewsServlet?action=downloadNewPic&newsPic1="+news.pic2+"";
						$(".img_box img").eq(1).attr("src",srcPath);
					}else{
						$(".img_box img").eq(1).css("display","block");
						$(".img_box img").eq(1).attr("src","../img/avatar_def.jpg");
					}
														
					$('#newsAddModal').modal('show');
				});
			});
		}
	}
	//Get classification details based on category id
	function getCategoryById(cateId,index) {
		if (cateId != null) {
			$.getJSON(path + "/CategoryServlet", {
				action : "getCategoryById",
				categoryid : cateId
			}, function(data) {
				if (data.retCode == 0) {
					$("#category_news" + index).text(data.retData.newsname);
				}else{
					$("#category_news" + index).text("Acquisition failed");
				}
			});
		}
	}
	
	/*Upload image*/
	function uploadPic(i){
		$("#editImg").val(i);
		if($(".img_ipt").eq(i-1).val()==""){
			alert("Please select an image");
		}else{
			var form=new FormData(document.getElementById("exampleModalLabel"));
			if($("#cop_title").text()=="Add news"){
				 $.ajax({
						url : path+"/NewsServlet?action=addNewsPic",
						type : "post",
						data : form,
						processData : false,
						contentType : false,
						success : function(data){						
							var data1=JSON.parse(data);				
							if(data1.retCode==0){
								setUploadImg(i+1);
								$("#newsPic"+i).val(data1.retData);
								if(i==1) {
									$(".img_box img").eq(i-1).css("display","block");
									var srcPath="<%=appPath%>/NewsServlet?action=downloadNewPic";
									$(".img_box img").eq(i-1).attr("src",srcPath);
								}
								if(i==2) {
									$(".img_box img").eq(i-1).css("display","block");	
									var srcpath1="<%=appPath%>/NewsServlet?action=downloadNewPics";
									$(".img_box img").eq(i-1).attr("src",srcpath1);
								}
							}
						},
						error : function(e) {
							alert("Upload failed, please try again");						
						}
					});
			}else{
				
				for(var j=0;j<2;j++){
					if(j!=(i-1)){
						$(".img_ipt").eq(j).val("");
					}
				}
				$.ajax({
					url : path+"/NewsServlet?action=updateNewPic",
					type : "post",
					data : form,
					processData : false,
					contentType : false,
					success : function(data){
						var data1=JSON.parse(data);
						if(data1.retCode==0){
							$("#newsPic"+i).val(data1.retData);
							if(i==1) {
								$(".img_box img").eq(i-1).css("display","block");
								var srcPath="<%=appPath%>/NewsServlet?action=downloadNewPic";
								$(".img_box img").eq(i-1).attr("src",srcPath);
								$(".img_ipt").eq(i - 1).val("");
							}
							if(i==2) {
								$(".img_box img").eq(i-1).css("display","block");	
								var srcpath1="<%=appPath%>/NewsServlet?action=downloadNewPics";
								$(".img_box img").eq(i-1).attr("src",srcpath1);
								$(".img_ipt").eq(i - 1).val("");
							}
						}
					},
					error : function(e) {
						alert("Upload failed, please try again");
						$('#newsAddModal').modal('hide');
					}
				});

			}

		}
	}
	
	// Add news events
	function addNews() {
		var result = checkAdd();
		if (result) {
			var data = {
				action : "addNews",
				title : $("#title").val(),
				source : $("#newsSource").val(),
				newsCategory : $("#newsCategory").val(),
				newsPic1 : $("#newsPic1").val(),
				newsPic2 : $("#newsPic2").val(),
				content : $("#newsContent").val()
			}
			$.getJSON(path + "/NewsServlet", data, function(data) {
				if (data.retCode == 0) {
					getNewsList();
					location.reload();
					$('#newsAddModal').modal('hide');
				} else {
					alert("Add failed, please try again");
					$('#newsAddModal').modal('hide');
				}
			});
		}
		$("#exampleModalLabel input[name]").val("");
	}
	// Add news field verification
	function checkAdd() {
		$("#exampleModalLabel .tip").text("");
		var title = $("#title").val();
		var newsSource = $("#newsSource").val();
		var content = $("#newsContent").val();
		if (title == "" || newsSource == "" || content == "") {
			$("#exampleModalLabel .tip").text("Please complete the news headline, source and content！");
			return false;
		} else if (($("#newImg1").val() != "")
				&& ($("#newImg1").css("display") == "block")) {
			$("#exampleModalLabel .tip").text("You selected picture 1, please submit it first or re-select it to be empty!");
			return false;
		} else if (($("#newImg2").val() != "")
				&& ($("#newImg2").css("display") == "block")) {
			$("#exampleModalLabel .tip").text("You selected Picture 2, please submit it or re-select it first!");
			return false;
		} else if (($("#newImg3").val() != "")
				&& ($("#newImg3").css("display") == "block")) {
			$("#exampleModalLabel .tip").text("You selected Picture 3, please submit it or re-select it first!");
			return false;
		}
		$("#exampleModalLabel .tip").text("");
		return true;
	}

	/*Delete news tip box*/
	function delNews(id) {
		$("#delId").val(id);
		$('.danger_alert').css("display", "block");
	}
	/* Undelete button click event*/
	function cancelDel() {
		$('.danger_alert').css("display", "none");
	}

	/*Add button click event*/
	function addNewsBtn() {
		$("#cop_title").text("Add news");
		$('#newsAddModal').modal('show');
		$(".btn_upimg").val("Upload");
		$(".imgaa").css("display", "none");
		setUploadImg(1);
	}

	/*Set the status of the first few pictures to add*/
	function setUploadImg(i) {
		$(".btn_upimg").css("display", "none");
		$(".img_ipt").css("display", "none");

		$(".btn_upimg").eq(i - 1).css("display", "block");
		$(".img_ipt").eq(i - 1).css("display", "block");
	}

	//Fuzzy search button click event
	function searchLike() {
		var likestr = $("#search_ipt").val();
		$('#ProgressModal').modal('show');
		$("#newsListBody").html("");
		if (likestr == "") {
			getNewsList();
			$('#newsAddModal').modal('hide');
		} else {
			$.getJSON(path + "/NewsServlet", {
				action : "getNewsByLike",
				searchLike : likestr
			}, function(data) {
				if (data.retCode == 0) {
					var newsList = data.retData;
					showNewsList(newsList);
					$('#ProgressModal').modal('hide');
				}else{
					alert("No query results！");
					getNewsList();
					$('#newsAddModal').modal('hide');
				}
			});
		}
	}
</script>
</head>
<body>
	<div id="header">
		<jsp:include page="../header.jsp" />
		<div class="container-fluid" id="content">
			<div class="row">
				<div class="col-md-2" id="left_nav">
					<jsp:include page="../left.jsp" />
				</div>
				<div class="col-md-10" id="right_content">
					<!--News management-->
					<form class="form-horizontal row" id="search_row">

						<div class="col-md-3">
							<div class="input-group" id="search_box">
								<input type="text" class="form-control" id="search_ipt"
									placeholder="Title / Source / Content"> <span class="input-group-btn">
									<button class="btn btn-default" type="button" id="searchBtn"
										onclick="searchLike()" class="search_btn orange_icon">
										<span class="glyphicon glyphicon-search input_icon"></span>
									</button>
								</span>
							</div>
							<!-- /input-group -->
						</div>
						<div class="col-md-offset-8 col-md-1 text-right">
							<button type="button" class="btn btn-default btn-sm orange_icon"
								id="addBtn" onclick="addNewsBtn()">
								<span class="glyphicon glyphicon-plus-sign"></span> Add
							</button>
						</div>
					</form>
					<table class="table table-bordered text-center" cellspacing="0"
						id="data_table">
						<thead>
							<td>Serial number</td>
							<td>Title</td>
							<td>Source</td>
							<td>Classifyid</td>
							<td>Readcount</td>
							<td>Commentcount</td>
							<td>Picture1</td>
							<td>Picture2</td>
							<td>Registertime</td>
							<td>Modifytime</td>
							<td>Content</td>
							<td>Operation</td>
						</thead>
						<tbody id="newsListBody" align="center">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- Delete warning box-->
	<div class="danger_alert alert alert-danger alert-dismissible fade in"
		role="alert">
		<h4>Delete warning</h4>
		<p>Are you sure you want to delete this user?</p>
		<p class="btn_tip">
			<button type="button" class="btn btn-xs btn-danger" id="sureDelNews">Delete</button>
			<button type="button" class="btn btn-xs btn-default"
				onclick="cancelDel()">Cancel</button>
			<input type="hidden" value="-1" id="delId">
		</p>
	</div>
	<!--Add category popup-->
	<div id="newsAddModal" class="modal fade bs-example-modal-sm"
		tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="cop_title">Add news</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="exampleModalLabel">
						<div class="form-group">
							<label for="title" class="col-md-2 control-label">Title:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="title" name="title">
							</div>
							<input type="hidden" value="-1" id="editId" name="editId">
						</div>
						<div class="form-group">
							<label for="newsSource" class="col-md-2 control-label">Source:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="newsSource"
									name="newsSource" value="BBC News">
							</div>
						</div>
						<div class="form-group">
							<label for="newsCategory" class="col-md-2 control-label">Classifyid:</label>
							<div class="col-md-6">
								<select id="newsCategory" name="newsCategory"
									class="form-control">
								</select>
							</div>
						</div>
						<div class="form-group img_box">
							<label class="col-md-2 control-label">image:</label>
							<div class="col-md-3 text-center">
									image1: <input type="file" class="form-control img_ipt"
									name="newsImg1" id="newImg1"> <input type="hidden"
									id="newsPic1" > <img class="imgaa" alt=""
									src="../img/avatar_def.jpg" id="news_img1"> <input
									type="button" class="btn btn-sm btn-danger btn_upimg"
									id="upload_btn1" value="Upload image 1" onclick="uploadPic(1)">
							</div>
							<div class="col-md-3 text-center">
									image2: <input type="file" class="form-control img_ipt"
									name="newsImg1" id="newImg1"> <input type="hidden"
									id="newsPic2" > <img class="imgaa" alt=""
									src="../img/avatar_def.jpg" id="news_img1"> <input
									type="button" class="btn btn-sm btn-danger btn_upimg"
									id="upload_btn1" value="Upload image 2" onclick="uploadPic(2)">
							</div>
							<input type="hidden" name="editImg" id="editImg" value="-1">
						</div>
						<div class="form-group">
							<label for="content" class="col-md-2 control-label">Content:</label>
							<div class="col-md-8">
								<textarea style="width: 100%; height: 200px" id="newsContent"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-6">
								<span class="tip" style="color: red"></span>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-default" data-dismiss="modal"
						form="exampleModalLabel" onclick="cancelDel()">
						<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
						Cancel
					</button>
					<button type="button" class="btn btn-default" id="sureAddOrEditBtn">
						<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../progress_bar.jsp"></jsp:include>
</body>
</html>