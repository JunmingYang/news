$(function(){
	$("#header .list-group>.list-group-item").removeClass("active");
	
	/* 禁用浏览器后退按钮 */
	if (window.history && window.history.pushState) {
		$(window).on('popstate', function() {
			window.history.pushState('forward', null, '#');
			window.history.forward(1);
		});
	}
	window.history.pushState('forward', null, '#');
	window.history.forward(1);
	
	
});
