<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	if (session.getAttribute("loginAdmin") == null) {
		response.sendRedirect("login.jsp");
	}
	String appPath = request.getContextPath();
	//Determine whether to log in. If the user is not logged in, redirect to the login.jsp page to log in.
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>User Management</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<!--Introducing a common css style file -->
<link href="<%=appPath%>/backend/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<%=appPath%>/backend/css/common.css" />
<link rel="stylesheet" href="<%=appPath%>/backend/css/index.css" />
<script src="<%=appPath%>/backend/jquery/jquery-1.11.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=appPath%>/backend/js/birthday.js"></script>
<script src="<%=appPath%>/backend/js/date_util.js"
	type="text/javascript"></script>
<script src="<%=appPath%>/backend/js/common.js" type="text/javascript"></script>
<script src="<%=appPath%>/backend/bootstrap/js/alert.js"
	type="text/javascript"></script>
<script>
	var path = "/news";

	$(function() {
		$("#header .list-group a").eq(0).addClass("active");
	});
	function showUserList() {
		$('#ProgressModal').modal('show');
		$("#userListBody").html("");
		// Get user list
		$.getJSON(path + "/UserServlet", {
			action : "getUserList"
		}, function(data) {	
			if (data.retCode == 0) {
				var userList = data.retData;
				listResultHandle(userList);
			} else {
				alert("User list acquisition failed");
				$('#ProgressModal').modal('hide');
			}

		});

		$("#addBtn").click(function() {
			$("#cop_title").text("Add user");
			$("#addAvatar").val("Upload");
			$("#username").removeAttr("readonly");
			$("#suer_pwd_box").css("display", "block");
			$("#curr_img").css("display", "none");
			$("#addAvatar").removeClass("btn-success");
			$("#addAvatar").removeAttr("disabled");
			$("#addAvatar").addClass("btn-danger");
			$("#myModal").modal('show');
		})
	}
	/*Processing the list of query results*/
	function listResultHandle(userList) {
		if(userList.length==0){
			$("#newsListBody").append('<h2 class="text-center" style="color:lightgray;font-size:30px;">Get result is empty</h2>');
			$('#ProgressModal').modal('hide');
		}else{
			for (var i = 0; i < userList.length; i++) {
				var user = userList[i];
				var userHtml = '<tr id="userTr'+i+'">' + '<td>'
						+ user.id
						+ '</td>'
						+ '<td>'
						+ user.username
						+ '</td>'
						+ '<td>'
						+ user.userpwd
						+ '</td>'
						+ '<td>'
						+ user.phone
						+ '</td>'
						+ '<td>'
						+ (user.sex == 1 ? "Male" : "female")
						+ '</td>'
						+ '<td>'
						+ user.nickname
						+ '</td>'
						+ '<td>'
						+ user.card
						+ '</td>'
						+ '<td>'
						+ (user.type == 1 ? "Available" : "Not available")
						+ '</td>'
						+ '<td>'
						+ user.registertime
						+ '</td>'
						+ '<td>'
						+ user.logintime
						+ '</td>'
						+ '<td class="icon">'
						+ '<div class="caozuo_grid row">'
						+ '<div class="col-md-4 col-md-offset-2  caozuo_update" id="editUser'+i+'"></div>'
						+ '<div class="col-md-4 caozuo_del"  onclick="deleteUser('
						+ user.id + ',' + i + ')"></div>' + '</div>' + '</td>'
						+ '</tr>';
	
				$("#userListBody").append(userHtml);
				$('#ProgressModal').modal('hide');
			}
			/*Modify user click events
			 */
			$(".caozuo_update").each(
					function(i) {
						$("#editUser" + i).click(
								function() {
											var user = userList[i];
											$("#userid").val(user.id);
											$("#cop_title").text("Modify users");
											$("#username").val(user.username);
											$("#username").attr("readonly",true);
											$("#password").val(user.userpwd);
											$("#suer_pwd_box").css("display", "none");
											$("#bindphone").val(user.phone);
											if (user.sex == 1) {
												$("#man").prop("checked","checked");
											} else {
												$("#women").prop("checked","checked");
											}
											$("#nickname").val(user.nickname);
											$("#card").val(user.card);
											if (user.userstate == 1) {
												$("#enable").prop("checked","checked");
											} else {
												$("#disable").prop("checked",	"checked");
											}
											$("#myModal").modal('show');
										});
	
							});
		}
	}

	function deleteUser(userId, i) {

		$("#delUserId").val(userId);
		$("#delIndex").val(i);
		$('.danger_alert').css("display", "block");
	}
	function cancelDel() {
		$('.danger_alert').css("display", "none");
	}
	/*Delete users*/
	function sureDel() {
		if ($("#delUserId").val() != -1) {
			$.getJSON(path + "/UserServlet?action=delUser", {
				userid : $("#delUserId").val()
			}, function(data) {
				if (data.retCode == 0) {

					if (delIndex != -1) {
						$('#userTr' + $("#delIndex").val()).remove();
					} else {
						location.reload();
					}
				}
				$('.danger_alert').css("display", "none");
			});
		}

	}

	// Add and modify user events
	function addUserInfo() {
		
		var result = checkAdd();
		if (result) {
				var data = {
					userid : $("#userid").val(),
					username : $("#username").val(),
					password : $("#password").val(),
					bindphone : $("#bindphone").val(),
					sex : $("#man").prop("checked") ? 1 : 0,
					nickname : $("#nickname").val(),
					card : $("#card").val(),
					userstate : $("#enable").prop("checked") ? 1 : 0,
					}
				if ($("#cop_title").text() == "Add user") {
					$.getJSON(path + "/UserServlet?action=addUser", data,
							function(data) {
								if (data.retCode == 0) {
									location.reload();
								} else if (data.retCode = 2004) {
									alert("Username already exists, please re-enter");
								} else {
									alert("Add failed, please try again");
								}
							});
				} else {//Modify users
					$.getJSON(path + "/UserServlet?action=updateUserInfo",
							data, function(data) {
								if (data.retCode == 0) {
									location.reload();
								} else {
									alert("Add failed, please try again");
								}
							});
				}		
		}
		$("#exampleModalLabel").reset();
	}
	// Add user field verification
	function checkAdd() {
		var nickname = $("#nickname").val().trim();
		var username = $("#username").val().trim();
		var password = $("#password").val().trim();
		var suer_password = $("#suer_password").val().trim();
		var bindphone = $("#bindphone").val().trim();
		var card = $("#card").val().trim();
		
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		var mycar = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
		
		if (nickname == "" || username == "" || password == "") {
			$("#exampleModalLabel .tip").text("Nickname, username and password are required!");
			return false;
		} else if ((password != suer_password)
				&& ($("#cop_title").text() == "Add user")) {
			$("#exampleModalLabel .tip").text("The two passwords are inconsistent");
			return false;
		} else if (bindphone != "") {
			if (!myreg.test(bindphone)) 
			{
				$("#exampleModalLabel .tip").text('Please enter a valid phone number！');
				return false;
			} else if (card != "") {
				if(!mycar.test(card)){
				$("#exampleModalLabel .tip").text('Please enter a valid ID number！');
				return false;					
				}			
			}
		} else if (card != "") {
				if(!mycar.test(card)){
				$("#exampleModalLabel .tip").text('please enter a valid ID number！');
				return false;					
				}			
			}
		$("#exampleModalLabel .tip").text("");
		return true;
	}

	/*Get administrator list*/
	function getAdminList() {
		// Get user list
		$("#adminListBody").html("");
		$.getJSON(
			path + "/AdminServlet",
				{
					action : "getAdminList"
				},function(data) {
					if (data.retCode == 0) {
						var adminList = data.retData;
						for (var i = 0; i < adminList.length; i++) {
							var admin = adminList[i];			
							var adminHtml = '<tr id="adminTr'+i+'">'
									+ '<td>'
									+ admin.id
									+ '</td>'
							 		+ '<td>'
									+ admin.adminname
									+ '</td>'
									+ '<td>'
									+ admin.adminpwd
									+ '</td>'
									+ '<td>'
									+ admin.registertime
									+ '</td>'
									+ '<td>'
									+ (admin.type == 1 ? "Available" : "Not available")
									+ '</td>'
									+ '<td class="icon">'
									+ '<div class="caozuo_grid row">'
									+ '<div class="col-md-4 col-md-offset-2  caozuo_update" id="updateAdmin'+i+'"></div>'
									+ '<div class="col-md-4 caozuo_del"  onclick="removeAdmin('
									+ admin.id
									+ ','
									+ i
									+ ','
									+ i
									+ ')"></div>'
									+ '</div>'
									+ '</td>'
									+ '</tr>';
							$("#adminListBody").append(adminHtml);
						}
						$(".caozuo_update").each(							
							function(i) {
								$("#updateAdmin" + i).click(
									function(){
										var admin = adminList[i];
										$("#adminModal").modal('show');															
											$("#adminid").val(admin.id);
											if (admin.state == 1) 
											{
												$("#adminEn").prop("checked",true);
											} 
											else
											{
												$("#adminDis").prop("checked",true);
											}															
								})
						})
					} else {
						alert("The administrator list is empty");
						$('#ProgressModal').modal('hide');
					}
				});
	}

	/*Modify administrator information*/
	function updateAdminSure() {
		if ($("#adminid").val() != -1) {
			$.getJSON(path + "/AdminServlet", {
				action : "updateAdmin",
				adminid : $("#adminid").val(),
				state : $("#adminEn").prop("checked") ? 1 : 0
			}, function(data) {
				if (data.retCode == 0) {
					getAdminList();
				} else {
					alert("Fail to edit");
				}
				$('#adminModal').modal('hide');
			});
		}
	}
	/*Remove from admin list*/
	function removeAdmin(adminId, index) {
		/* alert(adminId); */
		if (adminId != null) {
			$.getJSON(path + "/AdminServlet", {
				action : "removeAdmin",
				adminid : adminId
			}, function(data) {
				if (data.retCode == 0) {
					$("#adminTr" + index).remove();
					alert("Successfully removed");
				} else {
					alert("Removal failed");
				}
			});
		}
	}
	/*Fuzzy search event*/
	function searchLikeUser() {
		var likeStr = $("#search_ipt").val();
		$("#userListBody").html("");
		if (likeStr == "") {
			showUserList();
		} else {
			$.getJSON(
				path + "/UserServlet",
				{
					action : "getUserByLike",
					likeUserStr : likeStr
				},
				function(data) {
					if (data.retCode == 0) {
						var userList = data.retData;
						listResultHandle(userList);
					} else {
						alert("No result");
					}
				});
		}
	}
</script>


</head>
<body>
	<div id="header">
		<jsp:include page="header.jsp" />
		<div class="container-fluid" id="content">
			<div class="row">
				<div class="col-md-2" id="left_nav">
					<jsp:include page="left.jsp" />
				</div>
				<div class="col-md-10" id="right_content">
					<form class="form-horizontal row" id="search_row">
						<ul class="col-md-offset-1 col-md-4 nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#user"
								aria-controls="user" role="tab" data-toggle="tab">User</a></li>
							<li role="presentation"><a href="#admin"
								aria-controls="admin" role="tab" data-toggle="tab">Admin</a></li>
						</ul>
						<div class="col-md-3">
							<div class="input-group" id="search_box">
								<input type="text" class="form-control"
									placeholder="Nickname/username/phone" id="search_ipt"> <span
									class="input-group-btn">
									<button class="btn btn-default" type="button"
										class="search_btn">
										<span class="glyphicon glyphicon-search input_icon"
											aria-hidden="true" onclick="searchLikeUser()"></span>
									</button>
								</span>
							</div>
							<!-- /input-group -->
						</div>
						<div class=" col-md-offset-3 col-md-1 text-right">
							<button type="button" class="btn btn-default btn-sm orange_icon"
								id="addBtn">
								<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
								Add
							</button>
						</div>
					</form>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="user">
							<table class="table table-bordered text-center" cellspacing="0"
								id="data_table">
								<thead>
									<td>Serial number</td>
									<td>Username</td>
									<td>Password</td>
									<td>Phone number</td>
									<td>Gender</td>
									<td>Nickname</td>
									<td>ID card</td>
									<td>State</td>
									<td>Registertime</td>
									<td>Logintime</td>
									<td>Operation</td>
								</thead>
								<tbody id="userListBody" align="center">
								</tbody>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane" id="admin">
							<table class="table table-bordered text-center" cellspacing="0"
								id="data_table">
								<thead>
									<td>Serial number</td>
									<td>Username</td>
									<td>Password</td>
									<td>Add time</td>
									<td>State</td>
									<td>Operation</td>
								</thead>
								<tbody id="adminListBody">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Delete warning box-->
	<div class="danger_alert alert alert-danger alert-dismissible fade in"
		role="alert">
		<h4>Delete warning</h4>
		<p>Are you sure you want to delete this user?</p>
		<p class="btn_tip">
			<input type="hidden" value="-1" id="delUserId"> <input
				type="hidden" value="-1" id="delIndex">
			<button type="button" class="btn btn-xs btn-danger"
				onclick="sureDel()">Delete</button>
			<button type="button" class="btn btn-xs btn-default"
				onclick="cancelDel()">Cancel</button>
		</p>
	</div>

	<!-- Add User Add Modify Popup -->
	<div id="myModal" class="modal fade bs-example-modal-sm" tabindex="-1"
		role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="cop_title">Add user</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="exampleModalLabel">
						<div class="form-group">
							<label for="username" class="col-md-2 control-label">Username:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="username"
									name="username">
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-md-2 control-label">Password:</label>
							<div class="col-md-6">
								<input type="password" class="form-control" id="password"
									name="password">
							</div>
						</div>
						<div class="form-group" id="suer_pwd_box">
							<label for="suer_password" class="col-md-2 control-label">Confirm password:</label>
							<div class="col-md-6">
								<input type="password" class="form-control" id="suer_password"
									name="suer_password">
							</div>
						</div>
						<div class="form-group">
							<label for="bindphone" class="col-md-2 control-label">Phone number:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="bindphone"
									name="bindphone">
							</div>
						</div>
						<div class="form-group">
							<label for="sex" class="col-md-2 control-label">Gender:</label>
							<div class="col-md-6">
								<label class="radio-inline"> <input type="radio"
									name="sex" id="man" value="1" checked> Male
								</label> <label class="radio-inline"> <input type="radio"
									name="sex" id="women" value="0"> Female
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="nickname" class="col-md-2 control-label">Nickname:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="nickname"
									name="nickname" value=""> <input type="hidden"
									id="userid" name="userid" value="-1">
							</div>
						</div>
						<div class="form-group">
							<label for="card" class="col-md-2 control-label">ID card:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="card"
									name="card">
							</div>
						</div>
						<div class="form-group">
							<label for="userstate" class="col-md-2 control-label">State:</label>
							<div class="col-md-6">
								<label class="radio-inline"> <input type="radio"
									name="userstate" id="enable" value="1" checked> Available
								</label> <label class="radio-inline"> <input type="radio"
									name="userstate" id="disable" value="0"> Not available
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-6">
								<span class="tip"></span>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-default" data-dismiss="modal"
						form="exampleModalLabel">
						<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
						Cancel
					</button>
					<button type="button" class="btn btn-default"
						onclick="addUserInfo()">
						<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Add an administrator to modify the popup -->
	<div id="adminModal" class="modal fade bs-example-modal-sm"
		tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
		style="margin-top: 150px">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="cop_title">Modify administrator</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="adminModalLabel">
						<div class="form-group">
							<div class="col-md-6">
								<input type="hidden" id="adminid"
									name="adminid" value="-1">
							</div>
						</div>
						<div class="form-group">
							<label for="sex" class="col-md-2 control-label">State:</label>
							<div class="col-md-6">
								<label class="radio-inline"> 
								<input type="radio" name="state1" id="adminEn" value="1" checked> Available
								</label> <label class="radio-inline"> <input type="radio"
									name="state1" id="adminDis" value="0"> Not available
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-6">
								<span class="tip"></span>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-default" data-dismiss="modal"
						form="adminModalLabel">
						<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
						Cancel
					</button>
					<button type="button" class="btn btn-default"
						onclick="updateAdminSure()">
						<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="progress_bar.jsp"></jsp:include>

</body>
<script>
	showUserList();
	getAdminList();
</script>
</html>