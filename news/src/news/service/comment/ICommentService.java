package news.service.comment;

import java.util.List;

import news.pojo.Comment;

/**
 * Logical layer
 * @author Administrator
 *
 */
public interface ICommentService {

	List<Comment> showAll();

	int delete(int id);

	int update(Comment comment);

	List<Comment> select(String searchStr);

	List<Comment> selectByNewsId(int newsid);

	int add(Comment comment);
	
}
