package news.service.comment;


import java.util.List;

import news.pojo.Comment;
import news.service.BaseService;


public class CommentServiceImp extends BaseService implements ICommentService{

	@Override
	public List<Comment> showAll() {
		// TODO Auto-generated method stub
		return this.getCommentDao().showAll();
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return this.getCommentDao().delete(id);
	}

	@Override
	public int update(Comment comment) {
		// TODO Auto-generated method stub
		return this.getCommentDao().update(comment);
	}

	@Override
	public List<Comment> select(String searchStr) {
		// TODO Auto-generated method stub
		return this.getCommentDao().select(searchStr);
	}

	@Override
	public List<Comment> selectByNewsId(int newsid) {
		// TODO Auto-generated method stub
		return this.getCommentDao().selectByNewsId(newsid);
	}

	@Override
	public int add(Comment comment) {
		// TODO Auto-generated method stub
		return this.getCommentDao().add(comment);
	}
	
	


}
