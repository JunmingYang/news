package news.service;

import news.dao.admin.AdminDaoImp;
import news.dao.admin.IAdminDao;
import news.dao.classify.ClassifyDaoImp;
import news.dao.classify.IClassfiyDao;
import news.dao.comment.CommentDaoImp;
import news.dao.comment.ICommentDao;
import news.dao.news.INewsDao;
import news.dao.news.NewsDaoImp;
import news.dao.user.IUserDao;
import news.dao.user.UserDaoImp;

public class BaseService {

	private IAdminDao adminDao = new AdminDaoImp();
	
	private IUserDao userDao = new UserDaoImp();
	
	private IClassfiyDao classfiyDao = new ClassifyDaoImp();
	
	private INewsDao newsDao = new NewsDaoImp();
	
	private ICommentDao commentDao = new CommentDaoImp();
	
	/**
	 * @return the adminDao
	 */
	public IAdminDao getAdminDao() {
		return adminDao;
	}
	
	/**
	 * @return the userDao
	 */
	public IUserDao getUserDao() {
		return userDao;
	}
	
	/**
	 * @return the classfiyDao
	 */
	public IClassfiyDao getClassfiyDao() {
		return classfiyDao;
	}
	
	/**
	 * @return the newsDao
	 */
	public INewsDao getNewsDao() {
		return newsDao;
	}

	/**
	 * @return the commentDao
	 */
	public ICommentDao getCommentDao() {
		return commentDao;
	}

}
