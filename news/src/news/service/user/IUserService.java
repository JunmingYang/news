package news.service.user;

import java.util.List;

import news.pojo.User;
/**
 * Logical layer
 * @author Administrator
 *
 */
public interface IUserService {
	
	User login(String username, String userpwd);
	
	List<User> showAll();

	List<User> showByLike(String likeUserStr);
	
	int add(User user);
	
	User selectByName(String username);

	int delete(int userid);

	int update(User user);

	User selectById(int id);

	int logintime(String logintime, int id);

	User selectByPhone(String phone);
}
