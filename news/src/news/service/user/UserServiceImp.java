package news.service.user;

import java.util.List;


import news.pojo.User;
import news.service.BaseService;

public class UserServiceImp extends BaseService implements IUserService{

	@Override
	public User login(String username, String userpwd) {
		List<User> users = this.getUserDao().login(username, userpwd);
		if(null != users && !users.isEmpty()) {
			return users.get(0);
		}else {
			return null;		
		}
	}

	@Override
	public List<User> showAll() {
		// TODO Auto-generated method stub
		return this.getUserDao().showAll();
	}

	@Override
	public List<User> showByLike(String likeUserStr) {
		// TODO Auto-generated method stub
		return this.getUserDao().showByLike(likeUserStr);
	}

	@Override
	public int add(User user) {
		// TODO Auto-generated method stub
		return this.getUserDao().add(user);
	}

	@Override
	public User selectByName(String username) {
		List<User> users = this.getUserDao().selectByName(username);
		if(null != users && !users.isEmpty()) {
			return users.get(0);
		}else {
			return null;		
		}
	}

	@Override
	public int delete(int userid) {
		// TODO Auto-generated method stub
		return this.getUserDao().delete(userid);
	}

	@Override
	public int update(User user) {
		// TODO Auto-generated method stub
		return this.getUserDao().update(user);
	}

	@Override
	public User selectById(int id) {
		List<User> users = this.getUserDao().selectById(id);
		if(null != users && !users.isEmpty()){
			return users.get(0);
		}else {
			return null;			
		}
	}

	@Override
	public int logintime(String logintime, int id) {
		// TODO Auto-generated method stub
		return this.getUserDao().logintime(logintime, id);
	}

	@Override
	public User selectByPhone(String phone) {
		List<User> users = this.getUserDao().selectByPhone(phone);
		if(null != users && !users.isEmpty()){
			return users.get(0);
		}else {
			return null;			
		}
	}

}
