package news.service.admin;

import java.util.List;

import news.pojo.Admin;
import news.service.BaseService;

public class AdminServiceImp extends BaseService implements IAdminService{

	@Override
	public Admin login(String adminname, String adminpwd) {
		List<Admin> admins = this.getAdminDao().login(adminname, adminpwd);
		if(null != admins && !admins.isEmpty()) {
			return admins.get(0);
		}else {
			return null;		
		}
	}

	@Override
	public List<Admin> showAll() {
		// TODO Auto-generated method stub
		return this.getAdminDao().showAll();
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return this.getAdminDao().delete(id);
	}

	@Override
	public int update(int id, String type) {
		// TODO Auto-generated method stub
		return this.getAdminDao().update(id, type);
	}

}
