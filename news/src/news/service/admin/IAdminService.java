package news.service.admin;

import java.util.List;

import news.pojo.Admin;

/**
 * Logical layer
 * @author Administrator
 *
 */
public interface IAdminService {
	
	Admin login(String adminname, String adminpwd);
	
	List<Admin> showAll();
	
	int delete(int id);
	
	int update(int id, String type);
}
