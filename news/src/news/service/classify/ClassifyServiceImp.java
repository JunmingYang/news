package news.service.classify;

import java.util.List;

import news.pojo.Classify;
import news.service.BaseService;

public class ClassifyServiceImp extends BaseService implements IClassifyService{

	@Override
	public List<Classify> showAll() {
		// TODO Auto-generated method stub
		return this.getClassfiyDao().showAll();
	}

	@Override
	public int add(Classify classify) {
		// TODO Auto-generated method stub
		return this.getClassfiyDao().add(classify);
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return this.getClassfiyDao().delete(id);
	}

	@Override
	public int update(Classify classify) {
		// TODO Auto-generated method stub
		return this.getClassfiyDao().update(classify);
	}

	@Override
	public List<Classify> select(String cateStr) {
		// TODO Auto-generated method stub
		return this.getClassfiyDao().select(cateStr);
	}

	@Override
	public Classify selectById(int id) {
		List<Classify> classifies = this.getClassfiyDao().selectById(id);
		if(null != classifies && !classifies.isEmpty()) {
			return classifies.get(0);
		}else {
			return null;		
		}
	}


}
