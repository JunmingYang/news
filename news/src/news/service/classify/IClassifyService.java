package news.service.classify;

import java.util.List;

import news.pojo.Classify;

/**
 * Logical layer
 * @author Administrator
 *
 */
public interface IClassifyService {
	
	List<Classify> showAll();

	int add(Classify classify);

	int delete(int id);

	int update(Classify classify);

	List<Classify> select(String cateStr);

	Classify selectById(int id);
}
