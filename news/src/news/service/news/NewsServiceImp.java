package news.service.news;

import java.util.List;

import news.pojo.News;
import news.service.BaseService;

public class NewsServiceImp extends BaseService implements INewsService{

	@Override
	public List<News> showAll() {
		// TODO Auto-generated method stub
		return this.getNewsDao().showAll();
	}

	@Override
	public int add(News news) {
		// TODO Auto-generated method stub
		return this.getNewsDao().add(news);
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return this.getNewsDao().delete(id);
	}

	@Override
	public int update(News news) {
		// TODO Auto-generated method stub
		return this.getNewsDao().update(news);
	}

	@Override
	public List<News> select(String searchLike) {
		// TODO Auto-generated method stub
		return this.getNewsDao().select(searchLike);
	}

	@Override
	public News selectById(int id) {
		List<News> news = this.getNewsDao().selectById(id);
		if(null != news && !news.isEmpty()) {
			return news.get(0);
		}else {
			return null;			
		}
	}

	@Override
	public List<News> selectByClaId(String classifyId) {
		// TODO Auto-generated method stub
		return this.getNewsDao().selectByClaId(classifyId);
	}

	@Override
	public int updateCount(String id) {
		// TODO Auto-generated method stub
		return this.getNewsDao().updateCount(id);
	}

}
