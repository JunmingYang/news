package news.service.news;

import java.util.List;

import news.pojo.News;

/**
 * Logical layer
 * @author Administrator
 *
 */
public interface INewsService {
	
	List<News> showAll();

	int add(News news);

	int delete(int id);

	int update(News news);

	List<News> select(String searchLike);

	News selectById(int id);

	List<News> selectByClaId(String classifyId);

	int updateCount(String id);

}
