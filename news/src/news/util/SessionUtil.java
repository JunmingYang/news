package news.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;


public class SessionUtil {
	private static SessionUtil sessionUtil;
	private static Object obj = new Object();
	private SessionUtil() {
		
	}	
	public static SessionUtil getInstance() {
		if(null == sessionUtil) {
			synchronized (obj) {				
				sessionUtil = new SessionUtil();
			}
		}
		return sessionUtil;
	}
	
	public SqlSession getSession() {
		String resource= "SqlMapConfig.xml";
		InputStream is = null;
		try {
			is = Resources.getResourceAsStream(resource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(is);
		
		SqlSession session = sessionFactory.openSession(true);	
		return session;
	}
	
}
