package news.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import news.pojo.Admin;

public interface AdminMapper {
	
	@Select("select * from admin where adminname = #{adminname} and adminpwd = #{adminpwd}")
	public List<Admin> login(@Param("adminname")String adminname,@Param("adminpwd")String adminpwd);
	
	@Select("select * from admin")
	public List<Admin> showAll();
	
	@Delete("delete from admin where id = #{id}")
	public int delete(@Param("id")int id);
	
	@Update("update admin SET type = #{type} where id=#{id}")
	public int update(@Param("id")int id, @Param("type")String type);
}
