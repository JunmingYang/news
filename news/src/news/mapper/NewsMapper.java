package news.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import news.pojo.News;

public interface NewsMapper {
	
	@Select("select * from news")
	public List<News> showAll();
	
	@Insert("insert into news (title, source, classifyid, readcount, commentcount, pic1, pic2, content, registertime, modifytime) "
			+ "values (#{news.title}, #{news.source}, #{news.classifyid}, #{news.readcount}, #{news.commentcount}, #{news.pic1}, "
			+ "#{news.pic2}, #{news.content}, #{news.registertime}, #{news.modifytime})")
	public int add(@Param("news")News news);

	@Delete("delete from news where id = #{id}")
	public int delete(@Param("id")int id);

	@Update("update news set title = #{news.title}, source = #{news.source}, classifyid = #{news.classifyid}, pic1 = #{news.pic1}, "
			+ "pic2 = #{news.pic2}, modifytime = #{news.modifytime}, content = #{news.content} where id = #{news.id}")
	public int update(@Param("news")News news);
	
	@Select("select * from news where title like #{searchLike} or source like #{searchLike} or content like #{searchLike}")
	public List<News> select(@Param("searchLike")String searchLike);

	@Select("select * from news where id = #{id}")
	public List<News> selectById(@Param("id")int id);

	@Select("select * from news where classifyId = #{classifyId}")
	public List<News> selectByClaId(@Param("classifyId")String classifyId);

	@Update("update news set readcount = readcount + 1 where id = #{id}")
	public int updateCount(@Param("id")String id);
	
}
