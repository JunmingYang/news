package news.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import news.pojo.Classify;

public interface ClassifyMapper {
	
	@Select("select * from classify")
	public List<Classify> showAll();

	@Insert("insert into classify (newsname, registertime) values (#{classify.newsname}, #{classify.registertime})")
	public int add(@Param("classify")Classify classify);
	
	@Delete("delete from classify where id = #{id}")
	public int delete(@Param("id")int id);

	@Update("update classify set newsname = #{classify.newsname} where id = #{classify.id}")
	public int update(@Param("classify")Classify classify);

	@Select("select * from classify where newsname like #{cateStr}")
	public List<Classify> select(@Param("cateStr")String cateStr);

	@Select("select * from classify where id = #{id}")
	public List<Classify> selectById(@Param("id")int id);
}
