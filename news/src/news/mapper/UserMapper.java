package news.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import news.pojo.User;

public interface UserMapper {
	
	@Select("select * from user where username = #{username} and userpwd = #{userpwd}")
	public List<User> login(@Param("username")String username,@Param("userpwd")String userpwd);
	
	@Select("select * from user")
	public List<User> showAll();

	@Select("select * from user where nickname like #{likeUserStr} or phone like #{likeUserStr} or username like #{likeUserStr}")
	public List<User> showByLike(@Param("likeUserStr")String likeUserStr);
	
	@Insert("insert into user (username, userpwd, phone, sex, nickname, card, type, registertime, logintime) values "
			+ "(#{user.username}, #{user.userpwd}, #{user.phone}, #{user.sex}, #{user.nickname}, #{user.card}, #{user.type}, #{user.registertime}, #{user.logintime})")
	public int add(@Param("user")User user);
	
	@Select("select * from user where username = #{username}")
	public List<User> selectByName(@Param("username")String username);

	@Delete("delete from user where id = #{userid}")
	public int delete(@Param("userid")int userid);
	
	@Update("update user set username = #{user.username}, userpwd = #{user.userpwd}, phone = #{user.phone}, sex = #{user.sex}, nickname = #{user.nickname},"
			+ " card = #{user.card}, type = #{user.type} where id = #{user.id}")
	public int update(@Param("user")User user);

	@Select("select * from user where id = #{id}")
	public List<User> selectById(@Param("id")int id);

	@Update("update user set logintime = #{logintime} where id = #{id}")
	public int logintime(@Param("logintime")String logintime, @Param("id")int id);

	@Select("select * from user where phone = #{phone}")
	public List<User> selectByPhone(@Param("phone")String phone);
	
	
}
