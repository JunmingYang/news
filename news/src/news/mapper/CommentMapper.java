package news.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import news.pojo.Comment;


public interface CommentMapper {

	@Select("select * from comment")
	List<Comment> showAll();

	@Delete("delete from comment where id = #{id}")
	int delete(@Param("id")int id);
	
	@Update("update comment set newsid = #{comment.newsid}, content = #{comment.content}, type = #{comment.type} where id = #{comment.id}")
	int update(@Param("comment")Comment comment);

	@Select("select * from comment where content like #{searchStr}")
	List<Comment> select(@Param("searchStr")String searchStr);

	@Select("select * from comment where newsid = #{newsid}")
	List<Comment> selectByNewsId(@Param("newsid")int newsid);

	@Insert("insert into comment (newsid, userid, content, commenttime, type) values (#{comment.newsid}, #{comment.userid}, #{comment.content},  #{comment.commenttime}, #{comment.type})")
	int add(@Param("comment")Comment comment);
	
	
	
	
}
