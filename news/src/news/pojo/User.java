package news.pojo;
/**
 * 用户JavaBean
 * @author Administrator
 *
 */
public class User {
	
	private int id;
	private String username;
	private String userpwd;
	private String phone;
	private String sex;
	private String nickname;
	private String card;
	private String type;
	private String registertime;
	private String logintime;
	public User() {
		super();
	}
	public User(String username, String userpwd, String phone, String sex, String nickname, String card,
			String type, String registertime, String logintime) {
		super();
		this.username = username;
		this.userpwd = userpwd;
		this.phone = phone;
		this.sex = sex;
		this.nickname = nickname;
		this.card = card;
		this.type = type;
		this.registertime = registertime;
		this.logintime = logintime;
	}
	
	public User(int id, String username, String userpwd, String phone, String sex, String nickname, String card,
			String type) {
		super();
		this.id = id;
		this.username = username;
		this.userpwd = userpwd;
		this.phone = phone;
		this.sex = sex;
		this.nickname = nickname;
		this.card = card;
		this.type = type;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the userpwd
	 */
	public String getUserpwd() {
		return userpwd;
	}
	/**
	 * @param userpwd the userpwd to set
	 */
	public void setUserpwd(String userpwd) {
		this.userpwd = userpwd;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * @return the card
	 */
	public String getCard() {
		return card;
	}
	/**
	 * @param card the card to set
	 */
	public void setCard(String card) {
		this.card = card;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the registertime
	 */
	public String getRegistertime() {
		return registertime;
	}
	/**
	 * @param registertime the registertime to set
	 */
	public void setRegistertime(String registertime) {
		this.registertime = registertime;
	}
	/**
	 * @return the logintime
	 */
	public String getLogintime() {
		return logintime;
	}
	/**
	 * @param logintime the logintime to set
	 */
	public void setLogintime(String logintime) {
		this.logintime = logintime;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", userpwd=" + userpwd + ", phone=" + phone + ", sex="
				+ sex + ", nickname=" + nickname + ", card=" + card + ", type=" + type + ", registertime="
				+ registertime + ", logintime=" + logintime + "]";
	}
}
