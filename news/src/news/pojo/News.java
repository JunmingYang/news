package news.pojo;
/**
 * 新闻JavaBean
 * @author Administrator
 *
 */
public class News {
	
	private int id;
	private String title;
	private String source;
	private int classifyid;
	private int readcount;
	private int commentcount;
	private String pic1;
	private String pic2;
	private String registertime;
	private String modifytime;
	private String content;
	
	public News() {
		super();
	}
	
	
	public News(String title, String source, int classifyid, int readcount, int commentcount, String pic1, String pic2,
			String registertime, String modifytime, String content) {
		super();
		this.title = title;
		this.source = source;
		this.classifyid = classifyid;
		this.readcount = readcount;
		this.commentcount = commentcount;
		this.pic1 = pic1;
		this.pic2 = pic2;
		this.registertime = registertime;
		this.modifytime = modifytime;
		this.content = content;
	}

	
	public News(int id, String title, String source, int classifyid, String pic1, String pic2, String modifytime,
			String content) {
		super();
		this.id = id;
		this.title = title;
		this.source = source;
		this.classifyid = classifyid;
		this.pic1 = pic1;
		this.pic2 = pic2;
		this.modifytime = modifytime;
		this.content = content;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the classifyid
	 */
	public int getClassifyid() {
		return classifyid;
	}
	/**
	 * @param classifyid the classifyid to set
	 */
	public void setClassifyid(int classifyid) {
		this.classifyid = classifyid;
	}
	/**
	 * @return the readcount
	 */
	public int getReadcount() {
		return readcount;
	}
	/**
	 * @param readcount the readcount to set
	 */
	public void setReadcount(int readcount) {
		this.readcount = readcount;
	}
	/**
	 * @return the commentcount
	 */
	public int getCommentcount() {
		return commentcount;
	}
	/**
	 * @param commentcount the commentcount to set
	 */
	public void setCommentcount(int commentcount) {
		this.commentcount = commentcount;
	}
	/**
	 * @return the pic1
	 */
	public String getPic1() {
		return pic1;
	}
	/**
	 * @param pic1 the pic1 to set
	 */
	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}
	/**
	 * @return the pic2
	 */
	public String getPic2() {
		return pic2;
	}
	/**
	 * @param pic2 the pic2 to set
	 */
	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}
	/**
	 * @return the registertime
	 */
	public String getRegistertime() {
		return registertime;
	}
	/**
	 * @param registertime the registertime to set
	 */
	public void setRegistertime(String registertime) {
		this.registertime = registertime;
	}
	/**
	 * @return the modifytime
	 */
	public String getModifytime() {
		return modifytime;
	}
	/**
	 * @param modifytime the modifytime to set
	 */
	public void setModifytime(String modifytime) {
		this.modifytime = modifytime;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

}
