package news.pojo;
/**
 * 管理员JavaBean
 * @author Administrator
 *
 */
public class Admin {
	private int id;
	private String adminname;
	private String adminpwd;
	private String registertime;
	private String type;
	public Admin() {
		super();
	}
	public Admin(int id, String adminname, String adminpwd, String registertime, String type) {
		super();
		this.id = id;
		this.adminname = adminname;
		this.adminpwd = adminpwd;
		this.registertime = registertime;
		this.type = type;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the adminname
	 */
	public String getAdminname() {
		return adminname;
	}
	/**
	 * @param adminname the adminname to set
	 */
	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}
	/**
	 * @return the adminpwd
	 */
	public String getAdminpwd() {
		return adminpwd;
	}
	/**
	 * @param adminpwd the adminpwd to set
	 */
	public void setAdminpwd(String adminpwd) {
		this.adminpwd = adminpwd;
	}
	/**
	 * @return the registertime
	 */
	public String getRegistertime() {
		return registertime;
	}
	/**
	 * @param registertime the registertime to set
	 */
	public void setRegistertime(String registertime) {
		this.registertime = registertime;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	
}
