package news.pojo;
/**
 * 新闻评论JavaBean
 * @author Administrator
 *
 */
public class Comment {
	
	private int id;
	private int newsid;
	private int userid;
	private String content;
	private String commenttime;
	private String type;
	
	public Comment() {
		super();
	}
	
	public Comment(int id, int newsid, String content, String type) {
		super();
		this.id = id;
		this.newsid = newsid;
		this.content = content;
		this.type = type;
	}
	
	public Comment(int newsid, int userid, String content, String commenttime, String type) {
		super();
		this.newsid = newsid;
		this.userid = userid;
		this.content = content;
		this.commenttime = commenttime;
		this.type = type;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the newsid
	 */
	public int getNewsid() {
		return newsid;
	}
	/**
	 * @param newsid the newsid to set
	 */
	public void setNewsid(int newsid) {
		this.newsid = newsid;
	}
	/**
	 * @return the userid
	 */
	public int getUserid() {
		return userid;
	}
	/**
	 * @param userid the userid to set
	 */
	public void setUserid(int userid) {
		this.userid = userid;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the commenttime
	 */
	public String getCommenttime() {
		return commenttime;
	}
	/**
	 * @param commenttime the commenttime to set
	 */
	public void setCommenttime(String commenttime) {
		this.commenttime = commenttime;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	
}
