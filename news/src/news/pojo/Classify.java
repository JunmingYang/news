package news.pojo;
/**
 * 新闻分类JavaBean
 * @author Administrator
 *
 */
public class Classify {
	
	private int id;
	private String newsname;
	private String registertime;
	public Classify() {
		super();
	}
	public Classify(int id, String newsname, String registertime) {
		super();
		this.id = id;
		this.newsname = newsname;
		this.registertime = registertime;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the newsname
	 */
	public String getNewsname() {
		return newsname;
	}
	/**
	 * @param newsname the newsname to set
	 */
	public void setNewsname(String newsname) {
		this.newsname = newsname;
	}
	/**
	 * @return the registertime
	 */
	public String getRegistertime() {
		return registertime;
	}
	/**
	 * @param registertime the registertime to set
	 */
	public void setRegistertime(String registertime) {
		this.registertime = registertime;
	}

}
