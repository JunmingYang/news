package news.pojo;
/**
 * Json语句转化类
 * @author Administrator
 *
 */
public class Json {
	
	private String retCode;
	private Object retData;

	/**
	 * @return the retDate
	 */
	public Object getRetData() {
		return retData;
	}

	/**
	 * @param retDate the retDate to set
	 */
	public void setRetData(Object retData) {
		this.retData = retData;
	}

	/**
	 * @return the retCode
	 */
	public String getRetCode() {
		return retCode;
	}

	/**
	 * @param retCode the retCode to set
	 */
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	

	
}
