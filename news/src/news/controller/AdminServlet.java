package news.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;
import news.pojo.Admin;
import news.pojo.Json;
import news.pojo.User;
import news.service.admin.AdminServiceImp;
import news.service.admin.IAdminService;
import news.service.user.IUserService;
import news.service.user.UserServiceImp;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	
	public static void write(Json jsons, HttpServletResponse response) {
		JSONObject json = JSONObject.fromObject(jsons);
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write(json.toString());
	}
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset:utf-8");
		String action = request.getParameter("action");
		IAdminService adminService = new AdminServiceImp();

		
		//Get the action matching method
		//Administrator login
		if(action.equals("loginAdmin")) {
			//Get the username and password entered by the user and the verification code
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			String userValidate = request.getParameter("userValidate");
			HttpSession session = request.getSession();
			
			//Get the verification code displayed by the system
			String code = (String) session.getAttribute("ccode");
			
			//Find admin and user objects by username and password
			Admin admin = adminService.login(username, password);
			IUserService userService = new UserServiceImp();
			User user = userService.login(username, password);
			
			//Judge and return the json statement

			Json jsons = new Json();
			if(!code.equals(userValidate)) {
				jsons.setRetCode("1001");
			}else if (admin == null && user == null) {
				jsons.setRetCode("1002");
			}else if (admin == null && user != null) {
				jsons.setRetCode("1003");
			}else if (admin != null && user == null){
				jsons.setRetCode("0");
			}
			//Set the admin object in the session
			session.setAttribute(action, admin);
			
			AdminServlet.write(jsons, response);
		}
		
		//Get administrator list
		if (action.equals("getAdminList")) {
			List<Admin> admins = adminService.showAll();
			Json jsons = new Json();
			if(null != admins && !admins.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(admins);
			}else {
				jsons.setRetCode("1");
			}
			AdminServlet.write(jsons, response);
		}
		
		//Delete administrator
		if(action.equals("removeAdmin")) {
			String id = request.getParameter("adminid");
			int affectRow = adminService.delete(Integer.parseInt(id));
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			AdminServlet.write(jsons, response);
		}
		
		//Modify administrator
		if(action.equals("updateAdmin")) {
			String adminid = request.getParameter("adminid");
			String type = request.getParameter("state");
			int affectRow = adminService.update(Integer.parseInt(adminid), type);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			AdminServlet.write(jsons, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
