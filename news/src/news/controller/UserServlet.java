package news.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;
import news.pojo.Json;
import news.pojo.User;
import news.service.user.IUserService;
import news.service.user.UserServiceImp;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	public static void write(Json jsons, HttpServletResponse response) {
		JSONObject json = JSONObject.fromObject(jsons);
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write(json.toString());
	}
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset:utf-8");
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		IUserService userService = new UserServiceImp();
		
		//Get the action matching method
		//User display
		if(action.equals("getUserList")) {			
			List<User> users = userService.showAll();
			Json jsons = new Json();
			if(null != users && !users.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(users);
			}else {
				jsons.setRetCode("1");
			}
			JSONObject json = JSONObject.fromObject(jsons);
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
		}
		
		//Search user
		if(action.equals("getUserByLike")) {
			
			//Fuzzy query
			String likeUserStr = "%" + request.getParameter("likeUserStr") + "%";
			List<User> users = userService.showByLike(likeUserStr);
			Json jsons = new Json();
			if(null != users && !users.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(users);
			}else {
				jsons.setRetCode("1");
			}
			UserServlet.write(jsons, response);
		}
		
		//Add user
		if(action.equals("addUser")) {
			
			//Get all the parameters in the data
			String username = request.getParameter("username");
			String userpwd = request.getParameter("password");
			String phone = request.getParameter("bindphone");
			String sex = request.getParameter("sex");
			String nickname = request.getParameter("nickname");
			String card = request.getParameter("card");
			String type = request.getParameter("userstate");
			
			//Get system time and convert to the required format
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String registertime = sdf.format(date);
			String logintime = "None";
			Json jsons = new Json();
			
			//According to the username query to determine whether the name is the same
			User user = userService.selectByName(username);
			if(null == user) {
				user = new User(username, userpwd, phone, sex, nickname, card, type, registertime, logintime);
				int affectRow = userService.add(user);
				if(affectRow == 1) {
					jsons.setRetCode("0");
				}else {
					jsons.setRetCode("1");
				}
			}else {
				jsons.setRetCode("2004");
			}
			UserServlet.write(jsons, response);
		}
		
		//Delete users
		if(action.equals("delUser")) {
			String userid = request.getParameter("userid");
			int affectRow = userService.delete(Integer.parseInt(userid));
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			UserServlet.write(jsons, response);
		}
		
		//Modify users
		if(action.equals("updateUserInfo")) {
			
			//Get all the parameters in the data
			int id = Integer.parseInt(request.getParameter("userid"));
			String username = request.getParameter("username");
			String userpwd = request.getParameter("password");
			String phone = request.getParameter("bindphone");
			String sex = request.getParameter("sex");
			String nickname = request.getParameter("nickname");
			String card = request.getParameter("card");
			String type = request.getParameter("userstate");
			User user = new User(id,username, userpwd, phone, sex, nickname, card, type);
			int affectRow = userService.update(user);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			UserServlet.write(jsons, response);
		}
		
		//Search for user information based on id
		if(action.equals("getUserById")) {
			int id = Integer.parseInt(request.getParameter("userid"));
			User user = userService.selectById(id);
			Json jsons = new Json();
			if(null != user) {
				jsons.setRetCode("0");
				jsons.setRetData(user);
			}else {
				jsons.setRetCode("1");
			}
			UserServlet.write(jsons, response);
		}
		
		//Front-end user login
		if(action.equals("loginUser")) {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			User user = userService.login(username, password);
			
			//Get system time and convert to the required format
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String logintime = sdf.format(date);
			
			//Add login time based on user id
			Json jsons = new Json();
			if(null != user) {
				if(!user.getType().equals("0")) {
					
//					Generate a cookie and put the user id into it
					Cookie cookie = new Cookie("loginuser", String.valueOf(user.getId()));
//					Set the cookie expiration date to 30 days
					cookie.setMaxAge(24 * 30 * 60 * 60);
					response.addCookie(cookie);
					
					int affectRow = userService.logintime(logintime, user.getId());			
					if(affectRow == 1) {
						jsons.setRetCode("0");
					}else {
						jsons.setRetCode("1");
					}					
				}else {
					jsons.setRetCode("2004");
				}
			}else {
				jsons.setRetCode("1");
			}
			
			UserServlet.write(jsons, response);
		}
		
		//Front-end user phone number registration
		if(action.equals("registerUserByMobile")) {
			String nickname = request.getParameter("nickname");
			String username = request.getParameter("username");
			String sex = request.getParameter("sex");
			String card = request.getParameter("card");
			String phone = request.getParameter("bindphone");
			String password = request.getParameter("password");
			String code = request.getParameter("code");
			
			//Get system time and convert to the required format
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String registertime = sdf.format(date);
			String logintime = "None";
			String type = "1";
			
			//According to the username query to determine whether the name is the same
			Json jsons = new Json();
			User user = userService.selectByName(username);
			if(null == user) {
				
				//Get the verification code stored in the session
				String verificationcode = (String) session.getAttribute("code");
				if(verificationcode.equals(code)) {
					user = new User(username, password, phone, sex, nickname, card, type, registertime, logintime);
					int affectRow = userService.add(user);
					if(affectRow == 1) {
						jsons.setRetCode("0");
					}else {
						jsons.setRetCode("1");
					}					
				}else {
					jsons.setRetCode("2005");
				}
			}else {
				jsons.setRetCode("2004");
			}
			UserServlet.write(jsons, response);
		}
	}	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
	}

}
