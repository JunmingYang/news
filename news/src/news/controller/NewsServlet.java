package news.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import net.sf.json.JSONObject;
import news.pojo.Json;
import news.pojo.News;
import news.service.news.INewsService;
import news.service.news.NewsServiceImp;


/**
 * Servlet implementation class NewsServlet
 */
@WebServlet("/NewsServlet")
public class NewsServlet extends HttpServlet {
	/**
	 * Save the member variable of the image name
	 */
	private String fileName;

	public static void write(Json jsons, HttpServletResponse response) {
		JSONObject json = JSONObject.fromObject(jsons);
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write(json.toString());
	}
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset:utf-8");
		String action = request.getParameter("action");
		
		INewsService newsService = new NewsServiceImp();
		
		//Show all news
		if(action.equals("newsList")) {
			List<News> news = newsService.showAll();
			Json jsons = new Json();
			if(null != news && !news.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(news);
			}else {
				jsons.setRetCode("1");
			}
			NewsServlet.write(jsons, response);
		}
		
		//Add news
		if(action.equals("addNews")) {
			
			//Get the data passed in data
			String title = request.getParameter("title");
			String source = request.getParameter("source");
			int classifyid = Integer.parseInt(request.getParameter("newsCategory"));
			int readcount = 0;
			int commentcount = 0;
			String pic1 = request.getParameter("newsPic1");
			String pic2 = request.getParameter("newsPic2");
			String content = request.getParameter("content");
			
			//Get system time and convert to the required format
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String registertime = sdf.format(date);
			String modifytime = "None";
			
			News news = new News(title, source, classifyid, readcount, commentcount, pic1, pic2, registertime, modifytime, content);
			int affectRow = newsService.add(news);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			NewsServlet.write(jsons, response);
		}
		
		//Delete news
		if(action.equals("delNew")) {
			int id = Integer.parseInt(request.getParameter("id"));
			int affectRow = newsService.delete(id);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			NewsServlet.write(jsons, response);
		}
		
		//Modify news
		if(action.equals("updateNew")) {
			
			int id = Integer.parseInt(request.getParameter("id"));
			String title = request.getParameter("title");
			String source = request.getParameter("source");
			int classifyid = Integer.parseInt(request.getParameter("newsCategory"));
			String pic1 = request.getParameter("newsPic1");
			String pic2 = request.getParameter("newsPic2");
			String content = request.getParameter("content");
			
			//Get modify time
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String modifytime = sdf.format(date);
			News news = new News(id, title, source, classifyid, pic1, pic2, modifytime, content);
			int affectRow = newsService.update(news);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			NewsServlet.write(jsons, response);
		}
		
		//Fuzzy query
		if(action.equals("getNewsByLike")) {
			String searchLike = "%" + request.getParameter("searchLike") + "%";
			List<News> news = newsService.select(searchLike);
			Json jsons = new Json();
			if(null != news && !news.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(news);
			}else {
				jsons.setRetCode("1");
			}
			NewsServlet.write(jsons, response);
		}
		
		//Get news content based on id
		if(action.equals("getNewById")) {
			int id = Integer.parseInt(request.getParameter("id"));
			News news = newsService.selectById(id);
			Json jsons = new Json();
			if(null != news) {
				jsons.setRetCode("0");
				jsons.setRetData(news);
			}else {
				jsons.setRetCode("1");
			}
			NewsServlet.write(jsons, response);
		}
		
		//Loading image
		
		//Load the first image you just uploaded
		if(action.equals("downloadNewPic")) {
			String pic = request.getParameter("newsPic1");
			if(null == pic) {
				//Return io stream according to filename
				BufferedImage im=ImageIO.read(new File(fileName));
				ImageIO.write(im, "jpg", response.getOutputStream());
			}else {
				BufferedImage im=ImageIO.read(new File("d:" + File.separator + "image" + File.separator + pic));
				ImageIO.write(im, "jpg", response.getOutputStream());	
			}
		}
		
		//Load the second image you just uploaded
		if(action.equals("downloadNewPics")) {
			
			//Return io stream according to filename
			BufferedImage im=ImageIO.read(new File(fileName));
			ImageIO.write(im, "jpg", response.getOutputStream());	
		}
		
		//Upload image
		if(action.equals("addNewsPic")) {
			
			//Profile upload parameters
			DiskFileItemFactory disk = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(disk);			
			//Set request header parameters
			upload.setHeaderEncoding("utf-8");
			
			Json jsons = new Json();
			//Save image name
			String name = null;
			//Start accepting uploads
			try {			
				List<FileItem> items = upload.parseRequest(request);
				for (FileItem item : items){
					//Get the file name
					String fileName = item.getName();
					//Determine if the file is empty
					if (fileName!=null&&!fileName.equals("")) {
						name = fileName;
						//File path
						File file = new File("d:" + File.separator + "image" + File.separator + fileName);
						//Save the name of the image in the servlet
						this.fileName="d:" + File.separator + "image" + File.separator + fileName;
						item.write(file);
					} 
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsons.setRetCode("0");
			jsons.setRetData(name);
			NewsServlet.write(jsons, response);
			
		}
		
		//Update picture	
		if(action.equals("updateNewPic")) {
			
			//Profile upload parameters
			DiskFileItemFactory disk = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(disk);			
			//Set request header parameters
			upload.setHeaderEncoding("utf-8");
			
			Json jsons = new Json();
			//Save image name
			String name = null;
			//Start accepting uploads
			try {			
				List<FileItem> items = upload.parseRequest(request);
				for (FileItem item : items){
					//Get the file name
					String fileName = item.getName();
					//Determine if the file is empty
					if (fileName!=null&&!fileName.equals("")) {
						name = fileName;
						//File path
						File file = new File("d:" + File.separator + "image" + File.separator + fileName);
						//Save the name of the image in the servlet
						this.fileName="d:" + File.separator + "image" + File.separator + fileName;
						item.write(file);
					} 
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsons.setRetCode("0");
			jsons.setRetData(name);
			NewsServlet.write(jsons, response);			
		}
		
		//Get news by category
		if(action.equals("getNewsByCatId")) {
			String classifyId = request.getParameter("newsCategory");
			List<News> news = newsService.selectByClaId(classifyId);
			Json jsons = new Json();
			if(null != news && !news.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(news);
			}else {
				jsons.setRetCode("1");
			}
			NewsServlet.write(jsons, response);
		}
		
		//Add news views
		if(action.equals("readNewsCount")) {
			String id = request.getParameter("id");
			newsService.updateCount(id);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
