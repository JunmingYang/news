package news.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import net.sf.json.JSONObject;
import news.pojo.Json;
import news.pojo.User;
import news.service.user.IUserService;
import news.service.user.UserServiceImp;

/**
 * Servlet implementation class VerificationcodeServlet
 */
@WebServlet("/VerificationcodeServlet")
public class VerificationcodeServlet extends HttpServlet {
	
	public static void write(Json jsons, HttpServletResponse response) {
		JSONObject json = JSONObject.fromObject(jsons);
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write(json.toString());
	}
	
	private static final long serialVersionUID = 1L;
       
	//SMS verification request url
	private static String Url = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerificationcodeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset:utf-8");
		String action = request.getParameter("action");
		IUserService userService = new UserServiceImp();
		HttpSession session = request.getSession();
		
		//Query if this match is successful
		if(action.equals("searchVerificationcode")) {
					
			//Get the phone number to determine if the user exists
			String phone = request.getParameter("mobile");
			User user = userService.selectByPhone(phone);
			//Get the verification code stored in the session
			String verificationcode = (String) session.getAttribute("code");
			Json jsons = new Json();
			if(null != user) {
				jsons.setRetData(verificationcode);
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("2004");
			}
			VerificationcodeServlet.write(jsons, response);
		}
		
		//Send the verification code
		if(action.equals("sendVerificationCode")) {
			
			//Get the phone number
			String phone = request.getParameter("mobile");
			
			HttpClient client = new HttpClient(); 
			PostMethod method = new PostMethod(Url);

			client.getParams().setContentCharset("utf-8");
			method.setRequestHeader("ContentType","application/x-www-form-urlencoded;charset=utf-8");

			int mobile_code = (int)((Math.random()*9+1)*100000);

		    String content = new String("Your verification code is：" + mobile_code + "。Please don't reveal the verification code to others。");

			NameValuePair[] data = {//Submit a text message
				    new NameValuePair("account", "C55479137"), //View Username is Login User Center -> Verification Code SMS -> Product Overview -> APIID
				    new NameValuePair("password", "f27bc7d1669c6622152fdb3249990a14"),  //View the password, please log in to User Center->Verification Code SMS->Product Overview->APIKEY
				    //new NameValuePair("password", util.StringUtil.MD5Encode("Password")),
				    new NameValuePair("mobile", phone),
				    new NameValuePair("content", content),
			};
			method.setRequestBody(data);

			try {
				client.executeMethod(method);
				
				String SubmitResult =method.getResponseBodyAsString();

				//System.out.println(SubmitResult);

				Document doc = DocumentHelper.parseText(SubmitResult);
				Element root = doc.getRootElement();

				String code = root.elementText("code");
				Json jsons = new Json();
				if("40722".equals(code)){
					jsons.setRetCode("0");
					//Get session, save the verification code into the session
					session.setAttribute("code", String.valueOf(mobile_code));
					System.out.println(String.valueOf(mobile_code));
				}else {
					jsons.setRetCode("1");
				}
				VerificationcodeServlet.write(jsons, response);
				
			} catch (HttpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
