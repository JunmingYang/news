package news.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import news.pojo.Comment;
import news.pojo.Json;
import news.service.comment.CommentServiceImp;
import news.service.comment.ICommentService;

/**
 * Servlet implementation class UserEvaluateServlet
 */
@WebServlet("/UserEvaluateServlet")
public class UserEvaluateServlet extends HttpServlet {
	
	public static void write(Json jsons, HttpServletResponse response) {
		JSONObject json = JSONObject.fromObject(jsons);
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write(json.toString());
	}
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEvaluateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset:utf-8");
		String action = request.getParameter("action");
		ICommentService commentService = new CommentServiceImp();
		
		//Show all user reviews
		if(action.equals("userEvaluateList")) {
			List<Comment> comments = commentService.showAll();
			Json jsons = new Json();
			if(null != comments && !comments.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(comments);
			}else {
				jsons.setRetCode("1");
			}
			UserEvaluateServlet.write(jsons, response);
		}
		
		//Delete user comments
		if(action.equals("delUserEvaluate")) {
			int id = Integer.parseInt(request.getParameter("evaid"));
			int affectRow = commentService.delete(id);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			UserEvaluateServlet.write(jsons, response);
		}
		
		//Edit user comments
		if(action.equals("updateUserEvaluate")) {
			int id = Integer.parseInt(request.getParameter("evaid"));
			int newsid = Integer.parseInt(request.getParameter("newsid"));
			String style = request.getParameter("state");
			String content = request.getParameter("content");
			Comment comment = new Comment(id, newsid, content, style);
			int affectRow = commentService.update(comment);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			UserEvaluateServlet.write(jsons, response);
		}
		
		//Fuzzy query
		if(action.equals("searchEvaByLike")) {
			String searchStr = "%" + request.getParameter("searchStr") + "%";
			List<Comment> comments = commentService.select(searchStr);
			Json jsons = new Json();
			if(null != comments && !comments.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(comments);
			}else {
				jsons.setRetCode("1");
			}
			UserEvaluateServlet.write(jsons, response);
		}
		
		//Get a comment based on the news id
		if(action.equals("getEvaluatesByNewsid")) {
			int newsid = Integer.parseInt(request.getParameter("newsid"));
			List<Comment> comments = commentService.selectByNewsId(newsid);
			Json jsons = new Json();
			if(null != comments && !comments.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(comments);
			}else {
				jsons.setRetCode("1");
			}
			UserEvaluateServlet.write(jsons, response);
		}
		
		//Add comment
		if(action.equals("addUserEvaluate")) {
			int newsid = Integer.parseInt(request.getParameter("newsid"));
			int userid = Integer.parseInt(request.getParameter("userid"));
			String content = request.getParameter("content");
			String type = request.getParameter("type");
			
			//Get system time and convert to the required format
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String commenttime = sdf.format(date);
			
			Comment comment = new Comment(newsid, userid, content, commenttime, type);
			int affectRow = commentService.add(comment);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			UserEvaluateServlet.write(jsons, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
