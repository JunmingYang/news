package news.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import news.pojo.Classify;
import news.pojo.Json;
import news.service.classify.ClassifyServiceImp;
import news.service.classify.IClassifyService;

/**
 * Servlet implementation class ClassifyServlet
 */
@WebServlet("/CategoryServlet")
public class CategoryServlet extends HttpServlet {
	
	public static void write(Json jsons, HttpServletResponse response) {
		JSONObject json = JSONObject.fromObject(jsons);
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write(json.toString());
	}
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset:utf-8");
		String action = request.getParameter("action");
		IClassifyService classifyService = new ClassifyServiceImp();
		
		//Get the action matching method
		//Classification display
		if(action.equals("getCategoryList")) {
			List<Classify> classifies = classifyService.showAll();
			Json jsons = new Json();
			if(null != classifies && !classifies.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(classifies);
			}else {
				jsons.setRetCode("1");
			}
			CategoryServlet.write(jsons, response);
		}
		
		//Add category
		if(action.equals("addCategory")) {
			String newsname = request.getParameter("categoryname");
			Classify classify = new Classify();
			
			//Get the creation time of the format
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String registertime = sdf.format(date);
			
			classify.setNewsname(newsname);
			classify.setRegistertime(registertime);
			classifyService.add(classify);			
		}
		
		//Delete category
		if(action.equals("removeCategory")) {
			int id = Integer.parseInt(request.getParameter("categoryid"));
			int affectRow = classifyService.delete(id);
			Json jsons = new Json();
			if(affectRow == 1) {
				jsons.setRetCode("0");
			}else {
				jsons.setRetCode("1");
			}
			CategoryServlet.write(jsons, response);
		}
		
		//Modify category
		if(action.equals("updateCategory")) {
			int id = Integer.parseInt(request.getParameter("categoryid"));
			String newsname = request.getParameter("categoryname");
			Classify classify = new Classify();
			classify.setId(id);
			classify.setNewsname(newsname);
			classifyService.update(classify);
		}
		
		//Fuzzy lookup
		if(action.equals("getCategorysByLike")) {
			String cateStr = "%" + request.getParameter("cateStr") + "%";
			List<Classify> classifies = classifyService.select(cateStr);
			Json jsons = new Json();
			if(null != classifies && !classifies.isEmpty()) {
				jsons.setRetCode("0");
				jsons.setRetData(classifies);
			}else {
				jsons.setRetCode("1");
			}
			CategoryServlet.write(jsons, response);
		}
		
		//Get classification information based on id
		if(action.equals("getCategoryById")) {
			int id = Integer.parseInt(request.getParameter("categoryid"));
			Classify classify = classifyService.selectById(id);
			Json jsons = new Json();
			if(null != classify) {
				jsons.setRetCode("0");
				jsons.setRetData(classify);
			}else {
				jsons.setRetCode("1");
			}
			CategoryServlet.write(jsons, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
