package news.dao.classify;

import java.util.List;

import news.pojo.Classify;


/**
 * Data persistence layer
 * @author Administrator
 *
 */
public interface IClassfiyDao {
	
	List<Classify> showAll();

	int add(Classify classify);

	int delete(int id);

	int update(Classify classify);
	
	List<Classify> select(String cateStr);

	List<Classify> selectById(int id);
	

}
