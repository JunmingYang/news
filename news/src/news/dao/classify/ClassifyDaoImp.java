package news.dao.classify;


import java.util.List;

import org.apache.ibatis.session.SqlSession;

import news.mapper.ClassifyMapper;
import news.pojo.Classify;
import news.util.SessionUtil;


public class ClassifyDaoImp implements IClassfiyDao{
	
	private SessionUtil sessionUtil = SessionUtil.getInstance();
	private SqlSession session = sessionUtil.getSession();
	private ClassifyMapper mapper = session.getMapper(ClassifyMapper.class);
	
	@Override
	public List<Classify> showAll() {
		// TODO Auto-generated method stub
		return mapper.showAll();
	}

	@Override
	public int add(Classify classify) {
		// TODO Auto-generated method stub
		return mapper.add(classify);
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return mapper.delete(id);
	}

	@Override
	public int update(Classify classify) {
		// TODO Auto-generated method stub
		return mapper.update(classify);
	}

	@Override
	public List<Classify> select(String cateStr) {
		// TODO Auto-generated method stub
		return mapper.select(cateStr);
	}

	@Override
	public List<Classify> selectById(int id) {
		// TODO Auto-generated method stub
		return mapper.selectById(id);
	}
	
	
}
