package news.dao.user;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import news.mapper.UserMapper;
import news.pojo.User;
import news.util.SessionUtil;


public class UserDaoImp implements IUserDao{
	
	private SessionUtil sessionUtil = SessionUtil.getInstance();
	private SqlSession session = sessionUtil.getSession();
	private UserMapper mapper = session.getMapper(UserMapper.class);
	
	@Override
	public List<User> login(String username, String userpwd) {
		// TODO Auto-generated method stub
		return mapper.login(username, userpwd);
	}

	@Override
	public List<User> showAll() {
		// TODO Auto-generated method stub
		return mapper.showAll();
	}

	@Override
	public List<User> showByLike(String likeUserStr) {
		// TODO Auto-generated method stub
		return mapper.showByLike(likeUserStr);
	}

	@Override
	public int add(User user) {
		// TODO Auto-generated method stub
		return mapper.add(user);
	}

	@Override
	public List<User> selectByName(String username) {
		// TODO Auto-generated method stub
		return mapper.selectByName(username);
	}

	@Override
	public int delete(int userid) {
		// TODO Auto-generated method stub
		return mapper.delete(userid);
	}

	@Override
	public int update(User user) {
		// TODO Auto-generated method stub
		return mapper.update(user);
	}

	@Override
	public List<User> selectById(int id) {
		// TODO Auto-generated method stub
		return mapper.selectById(id);
	}

	@Override
	public int logintime(String logintime, int id) {
		// TODO Auto-generated method stub
		return mapper.logintime(logintime, id);
	}

	@Override
	public List<User> selectByPhone(String phone) {
		// TODO Auto-generated method stub
		return mapper.selectByPhone(phone);
	}
	
	

}
