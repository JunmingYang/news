package news.dao.user;

import java.util.List;
import news.pojo.User;

/**
 * 数据持久层
 * @author Administrator
 *
 */
public interface IUserDao {
	
	//用户登录
	List<User> login(String username, String userpwd);
	
	//展示全部
	List<User> showAll();
	
	//模糊查询
	List<User> showByLike(String likeUserStr);
	
	//添加
	int add(User user);
	
	//添加时判断是否重名
	List<User> selectByName(String username);
	
	//删除用户
	int delete(int userid);

	//更新用户
	int update(User user);

	//根据id查询用户
	List<User> selectById(int id);

	//登录时获取登录时间
	int logintime(String logintime, int id);

	//根据手机号判断用户是否存在
	List<User> selectByPhone(String phone);
}
