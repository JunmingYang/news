package news.dao.admin;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import news.mapper.AdminMapper;
import news.pojo.Admin;
import news.util.SessionUtil;


public class AdminDaoImp implements IAdminDao{
	
	private SessionUtil sessionUtil = SessionUtil.getInstance();
	private SqlSession session = sessionUtil.getSession();
	private AdminMapper mapper = session.getMapper(AdminMapper.class);
	
	@Override
	public List<Admin> login(String adminname, String adminpwd) {
		// TODO Auto-generated method stub
		return mapper.login(adminname, adminpwd);
	}

	@Override
	public List<Admin> showAll() {
		// TODO Auto-generated method stub
		return mapper.showAll();
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return mapper.delete(id);
	}

	@Override
	public int update(int id, String type) {
		// TODO Auto-generated method stub
		return mapper.update(id, type);
	}

}
