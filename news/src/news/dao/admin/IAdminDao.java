package news.dao.admin;

import java.util.List;

import news.pojo.Admin;

/**
 * Data persistence layer
 * @author Administrator
 *
 */
public interface IAdminDao {
	
	List<Admin> login(String adminname, String adminpwd);
	
	List<Admin> showAll();
	
	int delete(int id);
	
	int update(int id, String type);
}
