package news.dao.comment;

import java.util.List;

import news.pojo.Comment;


/**
 * Data persistence layer
 * @author Administrator
 *
 */
public interface ICommentDao {

	List<Comment> showAll();

	int delete(int id);

	int update(Comment comment);

	List<Comment> select(String searchStr);

	List<Comment> selectByNewsId(int newsid);

	int add(Comment comment);
	

}
