package news.dao.comment;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import news.mapper.CommentMapper;
import news.pojo.Comment;
import news.util.SessionUtil;


public class CommentDaoImp implements ICommentDao{
	
	private SessionUtil sessionUtil = SessionUtil.getInstance();
	private SqlSession session = sessionUtil.getSession();
	private CommentMapper mapper = session.getMapper(CommentMapper.class);
	
	@Override
	public List<Comment> showAll() {
		// TODO Auto-generated method stub
		return mapper.showAll();
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return mapper.delete(id);
	}

	@Override
	public int update(Comment comment) {
		// TODO Auto-generated method stub
		return mapper.update(comment);
	}

	@Override
	public List<Comment> select(String searchStr) {
		// TODO Auto-generated method stub
		return mapper.select(searchStr);
	}

	@Override
	public List<Comment> selectByNewsId(int newsid) {
		// TODO Auto-generated method stub
		return mapper.selectByNewsId(newsid);
	}

	@Override
	public int add(Comment comment) {
		// TODO Auto-generated method stub
		return mapper.add(comment);
	}
	
	

}
