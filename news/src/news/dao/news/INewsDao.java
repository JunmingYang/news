package news.dao.news;

import java.util.List;

import news.pojo.News;


/**
 * Data persistence layer
 * @author Administrator
 *
 */
public interface INewsDao {
	
	List<News> showAll();

	int add(News news);

	int delete(int id);

	int update(News news);

	List<News> select(String searchLike);

	List<News> selectById(int id);

	List<News> selectByClaId(String classifyId);

	int updateCount(String id);


}
