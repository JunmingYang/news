package news.dao.news;


import java.util.List;

import org.apache.ibatis.session.SqlSession;

import news.mapper.NewsMapper;
import news.pojo.News;
import news.util.SessionUtil;


public class NewsDaoImp implements INewsDao{
	
	private SessionUtil sessionUtil = SessionUtil.getInstance();
	private SqlSession session = sessionUtil.getSession();
	private NewsMapper mapper = session.getMapper(NewsMapper.class);
	
	@Override
	public List<News> showAll() {
		// TODO Auto-generated method stub
		return mapper.showAll();
	}

	@Override
	public int add(News news) {
		// TODO Auto-generated method stub
		return mapper.add(news);
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return mapper.delete(id);
	}

	@Override
	public int update(News news) {
		// TODO Auto-generated method stub
		return mapper.update(news);
	}

	@Override
	public List<News> select(String searchLike) {
		// TODO Auto-generated method stub
		return mapper.select(searchLike);
	}

	@Override
	public List<News> selectById(int id) {
		// TODO Auto-generated method stub
		return mapper.selectById(id);
	}

	@Override
	public List<News> selectByClaId(String classifyId) {
		// TODO Auto-generated method stub
		return mapper.selectByClaId(classifyId);
	}

	@Override
	public int updateCount(String id) {
		// TODO Auto-generated method stub
		return mapper.updateCount(id);
	}
	

	
}
